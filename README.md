This is a basic Tetris simulation.

# Algorithm

General strategires:
The nature of tetris is partly random. Even though we can argue that there can be some sort of "best move" for game state & piece, it's hard to search through all the possibility.

We utilize a heuristic approach down to 1 level of the game. Given a game state & a piece, we would check all the possible moves that can be made, which in turn generate many new grid. By using a list of features, we calculate the "estimated" survivalbility of each move. Finally, we pick the move that create the most "survivable" new grid.

Currently, the survivability is calculated so:

    survivability = sum[ weight_of_feature_i-th * value_of_feature_i-th ]

Realistically speaking, there can be more ways to calculate the survivablity, for example, in non-linear fashion

To determine the survivability, we would use machine learning.

There are currently 4 approaches: using genetic algorithm, using harmonic search, using stimualted annealing
and using particle swarm optimization.
 
## Features


### max column height (penalty)

Intuitively, raising the height too much might lead to a GAME OVER. This features gives penalty based on the row of the highest filled cell

For example, this grid will yield a penalty of `-8`

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    x  x  .  .  .  .  .  .  x  .  
    x  x  .  .  .  .  .  .  x  .  
    x  x  .  x  .  x  x  x  x  x  
    x  x  x  x  .  .  x  x  x  x  
    x  x  x  x  .  .  x  x  x  x  
    x  x  x  x  .  x  x  x  x  x 

### columns height (penalty)

Same as the above feature, however, we would gives penalty based on the summed height of all the column

The same grid as above would give a penalty of 

    -6 + -8 + -3 + -4 + 0 + -4 + -4 + -4 + -6 + -4 = -43

### hole count (penalty)

A __hole__ is defined as an empty cell with at least one filled cell above it. By intuition again, we don't want to have many holes because having hole on one row will make that row unclearable.

For example, this grid will yield a penalty of `-5` with 5 holes

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    x  x  .  .  .  .  .  .  x  .  
    x  x  x  .  .  .  .  .  x  .  
    x  x  x  x  x  x  x  x  x  x  
    x  x  x  x  .  .  x  x  x  x  
    x  x  x  x  .  .  x  x  x  x  
    x  x  x  x  .  x  x  x  x  x 

### weighted hole (penalty)

One more thing we take note while playing tetris: the more cells is above a hole, the harder it's cleared. Thus, in this feature, the penalty for one hole is the number of cells above it (up to the highest filled cell)

The 5 holes above will yield a penalty of

    -3 + -2 + -1 + -2 + -1 = -9

### row cleared (reward)

The sole feature that we give reward to a move. Because the goal is to lower the height, you essentially need to clear rows at some point, and the more row you can clear, the better.

This features will reward a move if it can clear some row, using data from the original grid (move not made) and the result grid (after the move)

For example, there will be a reward of `+3` in this situation

Original

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    x  x  .  .  .  .  .  .  x  .  
    x  x  x  .  .  .  .  .  x  .  
    x  x  x  x  o  x  x  x  x  x  
    x  x  x  x  o  .  x  x  x  x  
    x  x  x  x  o  x  x  x  x  x  
    x  x  x  x  o  x  x  x  x  x 

Result

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    x  x  .  .  .  .  .  .  x  .  
    x  x  x  .  .  .  .  .  x  .  
    x  x  x  x  o  .  x  x  x  x  

### Altitude difference (penalty)

We define the altitude is the height difference between the highest filled cell and the lowest empty, non-hole cell (it's empty and there's no filled cell above it). We would like to keep this value low so as not to create sky-scrapper like columns (those that are too high compared to its surrounding). When such column appears, it tends to divide the grid into 2 halves, making it harder to be filled.

For example, this grid would have an altitude difference of `-11`

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  .  .  
    .  x  .  .  .  .  .  .  x  .  
    .  x  .  .  .  .  .  .  x  .  
    x  x  .  x  .  x  x  x  x  x  
    x  x  x  x  .  x  x  x  x  x  
    x  x  x  x  .  x  x  x  x  x  
    x  x  x  x  .  x  x  x  x  x 

### max well depth (penalty)

A well is defined as a group of continuos, vertical empty cells that that lies between columns of filled cells or the wall. By intuition, if the well depth is too big, you can't fill it without leaving holes, unless you have a I block. Though we penalize all depth, it's arguable that well depth of 2, 3 or 4 is still acceptable.

For example, there are `4` wells in this grid and the max well depth penalty is `-4`

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  x  .  .  .  .  .  x  .  
    x  .  x  .  .  .  .  .  x  .  
    x  .  x  .  .  .  .  .  x  .  
    x  x  x  .  x  .  .  .  x  .  
    x  x  x  x  x  .  x  x  x  x  

### well depth (penalty)

In this features, we penalize each well based on its depth and then sum all up

The above grid would yield a penalty of

    -2 + -1 + -1 + -4 = -8

### cell count (penalty)

We count filled cells here. If we let this count goes up too much, it would mean we are stacking blocks but not able to clear them. Definitely not a good sign

The above grid would have a penalty of

    -4 + -2 + -5 + -1 + -2 + 0 + -1 + -1 + -5 + -1 = -22

### Weighted cell count (penalty)

This features gives penalty to cells based on how high it is. For example, a filled cell in the bottom row (1st row) would gives penalty of `-1`, while a filled cell on the 2nd row will gives a penalty of `-2` and so on. This mean the higher we stack the blocks, the faster our penalty will go up.

For example:

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  x  .  .  .  .  .  .  .  
    .  .  x  .  .  .  x  x  .  .  
    x  .  x  .  .  .  x  x  .  x  

This grid have a penalty of

    -1*1 + -1*( 1+2+3 ) + -1*( 1+2 ) + -1*( 1+2 ) + -1*1 = -14

### Row transition (penalty)

Consider the outside of the game grid "filled", we define a transition as a movement from a filled cell to an empty one, or vice versa. Row transition counts the number of transitions on each row, sum them up and give penalty accordingly

For example, the grid above gives a penalty of

    ( -1 + -1 + -1 + -1 + -1 + -1) + ( -1 + -1 + -1 + -1 + -1 + -1 ) + ( -1 + -1 + -1 + -1 ) = -16

### Column transition (penalty)

Similar to row transition, column transition counts the number of transitions for each columns.

For example:

    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  x  .  .  .  .  .  .  .  
    .  .  .  x  .  .  x  x  .  .  
    x  .  x  x  .  .  x  x  .  x  

gives a penalty of

    ( -1 ) + ( -1 ) + ( -1 + -1 + -1 ) + ( -1 ) + ( -1 ) + ( -1 ) + ( -1 ) + ( -1 ) +( -1 ) = -11

-------------

## Genetic algorithm

### How to define an entity's fitness ?
Some observation:

* The ultimate goal is still to __clear as many rows as possible__, so the fitness should be __proportional to no of row cleared__
* Another good metric to look out for is __how close the piece/row ratio is to 2.5__. The smaller the difference, the better -> fitness is __inversely proportional to (ratio - 2.5)__

For now, we use:

    fitness = no_of_row_cleared / ( 1 + ratio - 2.5 )

-------------

## Thoughts:

@Khiem: on genetic algo:
How do we converge to the solution fast? What I found from the papers is that we should play it with a smaller grid, or a harder version of the game (more % for S & Z pieces)

-------------

## Testing feature's correctness

Use the UnitTestByKhiem.java to construct a sample grid, print it out & verify the features

------------------------------------------------------------

# Development guide

## Generics

### Files
* State.java                        keep tracks of the grid, next pieces...
* PlayerSkeleton.java               a player that plays an actual game
* DataCollector.java                spawn multiple PlayerSkeleton and gather average stats
* UnitTestByKhiem.java              used for testing codes (mostly features)
* Features.java                     well... features :)
* TetrisLogger                      handle all the logging
* LogFormatter                      log formatting
* Genetic                           genetic algo

### The pieces
The pieces are given in the following order (read from left -> right)

             *                                                          
             *       *        *       *                                 
    **       *       *        *       **        **       **             
    **       *       **      **       *        **         **            
                                                                        
    [0]      [1]     [2]     [3]      [4]      [5]       [6]                          

## Adding algorithm

Create a separate file called __AlgoBy(your name).java__, for example, `AlgoByKhiem`, `AlgoByTho`
Ideally, put this as a static class
Specify a function:

    public static int pickMove(State gameState, int[][] legalMoves, List<Double> weights)

In the main PlayerSkeleton, we will call something like

    return AlgoByTho.pickMove(gameState, legalMoves, feature_weights);

To get the algorithm result. Any necessary request for additional variables, ask Khiem.

## Classes

### State
This class keep track of one tetris game. We tweaked it to be able to create a hard game with many more S (no.5) and Z (no.6) pieces.

    public State(boolean isHardGame, double probability)

generates a hard game with S and Z pieces's chance at `probability/2` and other pieces's chance at `(1-probability)/5`

    public State()

generates a normal game with each piece's chance at `1/7`

Also, the static variables `ROWS` and `COLS` can be changed to make the game board smaller

Notes from ealier:

* The board state is stored in field (a double array of integers) and
is accessed by getField().  Zeros denote an empty square.  Other values denote
the turn on which that square was placed.

* NextPiece (accessed by getNextPiece)
contains the ID (0-6) of the piece you are about to play.

* Moves are defined by two numbers: the SLOT, the leftmost column of the piece and
the ORIENT, the orientation of the piece.  
* Legalmoves gives an nx2 int array containing the n legal moves.
* A move can be made by specifying the two parameters as either 2 ints, an int array of length 2, or a single int specifying the row in the legalMoves array corresponding to the appropriate move.

### PlayerSkeleton
Create one PlayerSkeleton object to play one game. Once done, it returns no of lines cleared & pieces used.
PlayerSkeleton implements Callable interface so multiple instances can be spawned in parallel.

    PlayerSkeleton(String instance_name, List<Double> feature_weights,
                   boolean show_GUI, boolean more_hard_pieces,
                   double s_and_z_chance, int mili_between_moves)

    public List<Integer> call() //[row_cleared, piece_used]

### DataCollector
One DataCollector spawns multiple PlayerSkeleton (no UI) to play the game and get average performance of each.
DataCollector also implements Callable interface so it can be spawned in parallel, if needed

    DataCollector(String collector_name, List<Double> feature_weights,
                  boolean more_hard_pieces, double s_and_z_chance, int number_of_games)

    public List<Double> call() //[average_row_cleared, average_piece_used, average_ratio]

### TetrisLogger
I would like to be able to trace through what happen on individual run of a game, as well as each set of weights & generation, etc... so each instance has its own log file

To be used to log things. It can log both `string` values & `int[][]` (game grid)

    public TetrisLogger(String name)

* Use `PlayerSkeleton.LOGGER.info('message')` to log high level information (describe a move, for example)

* Use `PlayerSkeleton.LOGGER.debug('message')` to log low level information (i.e: print out parameters of a move)

For example

    PlayerSkeleton.LOGGER.info('Piece id= 2, Picked move no.5')
    ---------------------------------------------------------
    @INFO    Piece id= 2, Picked move no.5

or

    PlayerSkeleton.LOGGER.debug(gameState.getField())
    ---------------------------------------------------------
    @DEBUG
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  .  .  
    .  .  .  .  .  .  .  .  x  .  
    .  .  .  .  .  .  .  .  x  .  
    .  .  .  .  .  .  .  .  x  x 

### Genetic
Runs the genetic algorithm

    java Genetic min_row_requirement min_ratio_requirement max_generation_no s_and_z_chance selection_cutoff_threshold mutation_chance multiple_gene_mutation_chance

#### 1. Initialization
A population of 16 Individuals is created from 16 different dnas. Some dna is controlled (given by us), the rest are random

Related functions

    initialize_population

#### 2. Live one generation
For each dna, run ~10 game and get average stats for each, then calculate the fitness of each dna

Related functions

    live
    Individual.fitness

#### 3. Breeding and mutating
Select 2 random individuals based on their fitness (individual with high fitness should get more chance) and cross breeding them. Mutate this new child if chance permits.
Repeat 16 times to generate a new population

Related functions

    random_selection
    breed
    mutate

#### 4. Stop & output
When the population satisfy a certain criteria (no of cleared rounds > X or the no of generation > Y), stop and print out the weight sets (dnas)

Related functions

    is_qualified
    genetic_algorithm

-------------

# Simulation

### Server
Algorithms are left to run on a server. To view

    ssh mark@hostname
    screen -r
    ...
    Ctrl + A, Ctrl + D -> detacth the console

Ask Khiem for details about this server

----------------------------------------------------------------------------------------------------------------
### Particle Swarm Optimization (PSO)

Command to run the PSO algorithm: java PSO min_rows_cleared_requirement min_ratio_requirement max_iteration_no 
s_and_z_chance terminate_size c1 c2

### 1 initialize_swarm()
A swarm of N particles is initialized. 
    1. Each particle is located in a M-dimensional space by its M coordinations.
        Here, N is the population of test cases and M is the number of weight set.
    2. Each particle has its origin velocity = 0 in all dimensions.
    3. Each particle has its personal record of the best position it has passed through
        Data is stored in best_personal_position.
        At first, the best personal position of each particle is its initialized position.
    N = 64, M = 12.

### 2 run_pso_algorithm()
After being initialized, the swarm then goes through the loop:
    1. find_volume(swarm): find the current volume of the swarm.
        Return a List<Double> of maximum sizes of the volume in every dimension
    2. run_iteration(swarm): for each particles (set of weights), use DataCollector to run for about 10 times
        Record the aver_rows_cleared, aver_ratio, performance (fitness value)
    3. main loop: check whether the interation_no exceeds max_iteration_no and the volume is smaller or equal to the terminate_size or not. 
    If false, continue to 4. If true, go to 9
        function: is_qualified(volume)
    4. update_swarm(swarm, best_personal_position): update position of every particle in the swarm
        If this current position's performance is not good as the best_personal_position of the particle, come back to its best person position.
    5. find_best_global_position(best_global_postion, swarm): find the position with the best performance that the algorithm has ever gone through.
        record the position and its performance in best_global_position.
    6. find_best_personal_position(best_personal_position, swarm): update the best personal position of every particle
        record the list of best personal positions in best_personal_position
    7. find_velocity(velocity, swarm, best_global_position, best_personal_position): find the new velocity for every particle
         the velocity consists of:
        - interial = old velocity value
        - personal influence = c1 * (best personal position - current position)
                                * (1 - current performance/best personal performance)
        - global influence = c2 * (best global position - current position)
                                * (1 - current performance/best global performance)
        velocity = old velocity + personal influence + global influence
    8. find_new_position(swarm, velocity): find the new position of every particle:
        new position = current position + velocity
        then go to 3 (main loop)
    9. Terminate the algorithm

-----------------

# Logging
    Currently, PSO use 3 files to log data
    1. srcparticle_swarm_optimization.log: performances of all particles in each iteration
    2. srcperformance.log: log the vaer_rowscleared, aver_ratio and aver_performance of every iteration
    3. srcweightset.log: log the performance and weightset of best global position in every iteration             