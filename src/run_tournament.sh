# #!/bin/bash
username=$1
password=$2
machine="access4"

# !/usr/bin/expect
expect <<- DONE
  # upload yourself

  spawn ssh $username@tembusu.comp.nus.edu.sg
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"

  expect "@*"
  send -- "ssh -oStrictHostKeyChecking=no $machine\n"
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"

  # go to the source folder for that node
  expect "@*"
  send -- "cd $machine/src\n"

  # clear the logs
  expect "@*"
  send -- "rm ../log/*.*\n"

  # clear the compiled code
  expect "@*"
  send -- "rm *.class\n"

  # compile tournament code
  expect "@*"
  send -- "javac Tournament.java\n"

  # # open screen
  # expect "@*"
  # send -- "screen\n"

  # # run code
  # expect "@*"
  # send -- "java Tournament\n"
  
  # # wait 10s before detaching console
  # set timeout 60
  # send -- "\01\04"

  # # exit from the compg terminal
  # expect "@*"
  # send -- "exit\n"

  # spawn "scp -r ./src $username@tembusu.comp.nus.edu.sg:~/$machine/src "
  # expect "*?assword:*" # will be prompted for password
  # send -- "$password\n"

  # ssh into the compute node
  

  # exit from the access terminal
  # expect "@*"
  # send -- "exit\n"
  expect eof
DONE