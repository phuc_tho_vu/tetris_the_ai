import java.util.*;
import java.lang.Math.*;
import java.text.*;

public class PSO {
    private static TetrisLogger LOGGER = new TetrisLogger("pso");
    private static TetrisLogger DATA = new TetrisLogger("pso_performance");
    private static TetrisLogger WEIGHTSET = new TetrisLogger("weightset");
    private static TetrisLogger TEST = new TetrisLogger("test");
    private static DecimalFormat df = new DecimalFormat("#.#####");
    private static List<Individual> swarm = new ArrayList<Individual>();
    private static List<List<Double>> velocity = new ArrayList<List<Double>>();
    private static List<List<Double>> sample_position = new ArrayList<List<Double>>();
    private static List<Individual> best_personal_position = new ArrayList<Individual>();
    private static Individual best_global_position = new Individual();
    private static List<Double> volume = new ArrayList<Double>(); 
    private static final int SWARM_SIZE = 216;
    private static final int WEIGHTSET_SIZE = 12;
    private static int min_row_cleared_requirement;
    private static Double min_ratio_requirement;
    private static int max_iteration_no;
    private static Double s_and_z_chance;
    private static boolean is_hard;
    private static int instance_per_individual;
    private static boolean log_to_screen = false;
    private static Double terminate_size;
    private static Double interia_weight = 0.729;
    private static Double cognitive_weight = 1.49445; 
    private static Double social_weight = 1.49445;

    public static void initialize_swarm () {
        // initialize sample velocity usind data from last run
        Double[] sample0 = {-59.35997934749503, 3776.1314060133054, 9241.39017269405, 1823.7464503097144, 315.971398581896, -14342.879477474295, -6275.5993424980825, 6551.856107025566, -14314.95149949724, 2619.869698754609, 6741.138393135674, 9934.405376137558};
        sample_position.add(Arrays.asList(sample0));
        Double[] sample1 = {21.539335131606947, 1026.7623146707917, 650.0985909996524, 1864.8171309579059, 1675.5379939835739, 18.716353267202642, 44.86745937085246, 278.17959023057443, 533.2318298257198, 1818.9823784855855, 796.4315105698482, 1789.4387175273848};
        sample_position.add(Arrays.asList(sample1));
        Double[] sample2 = {1.0033504132885307, 0.04654395692991402, 0.7572571171718027, -0.2240531386958189, -0.31095897391154764, -0.33900214870053247, 0.016480117948880907, -0.4270878640104353, -0.4518649317203462, 0.18480309961135813, 0.6509102372092502, 0.06739755219765253};
        sample_position.add(Arrays.asList(sample2));
        Double[] sample3 = {0.005024002907486094, 0.03584626312330467, 0.029617592820746885, 0.02622952872711785, 0.069344026603861, -0.0032058218993562078, -0.0016759518765389688, 0.020380928471805625, -0.0568465550709089, 0.01961560637220734, 0.030093080027649672, 0.026817019994760882};
        sample_position.add(Arrays.asList(sample3));
        Double[] sample4 = {1.8915091548767646E-4, 0.008986796242239093, 0.005690482698220318, 0.01632170285496836, 0.014665008635741475, 1.636019908263987E-4, 3.927119862442661E-4, 0.0024345158280838263, 0.004666798778853735, 0.015920784218280318, 0.006971196771497768, 0.015662134520206834};
        sample_position.add(Arrays.asList(sample4));

        /*
        for (int i=0; i<sample_position.size(); i++) {
            swarm.add(new Individual(sample_position.get(i), i));
        }*/

        Double[][] initial_position = initialize_position(SWARM_SIZE, WEIGHTSET_SIZE);

        // initialize positions of particles 
        for (int i=0; i<SWARM_SIZE; i++) {
            swarm.add(new Individual(Arrays.asList(initial_position[i]), i));
        }

        // initialize best_personal_position 
        best_personal_position = swarm;

        // initialize velocity of particles
        for (int i=0; i<SWARM_SIZE; i++) {
            velocity.add(new ArrayList<Double>());
            for (int j=0; j<WEIGHTSET_SIZE; j++) {
                velocity.get(i).add(0.0);
            }
        }

        return;
    }

    public static List<Individual> calculate_performance(List<Individual> swarm, int iteration) {
        if (log_to_screen) {
            LOGGER.info( String.format("Iteration: %-20s", iteration) );
            LOGGER.info( String.format("%-30s %-20s %-20s %-20s", "Particle_name", "Avg rows cleared", "Avg ratio", "Performance") );
        } else {
            LOGGER.debug( String.format("Iteration: %-20s", iteration) );
            LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", "Particle_name", "Avg rows cleared", "Avg ratio", "Performance") );
        }

        for (Individual particle : swarm) {
            String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);

            DataCollector collector = new DataCollector(particle_name, particle.position, 
                                                is_hard, s_and_z_chance, instance_per_individual);
            particle.set_performance( collector.call() );
        }

        Collections.sort( swarm, new IndividualComparator() );


        for (Individual particle : swarm) {
            if (log_to_screen) {
                String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);
                LOGGER.info( String.format("%-30s %-20s %-20s %-20s", particle_name, df.format(particle.average_row_cleared), 
                    df.format(particle.average_ratio), df.format(particle.performance)) );
            } else {
                String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);
                LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", particle_name, df.format(particle.average_row_cleared), 
                    df.format(particle.average_ratio), df.format(particle.performance)) );
            }
        }
        return swarm;
    }

    public static void run_pso_algorithm () {
        int iteration = 0;
        DATA.info( String.format("%-20s %-20s %-20s", "Iteration", "Avg rows cleared", "Avg ratio") );
        WEIGHTSET.info(String.format("%-20s %-20s ", "RowsCleared", "weightSet"));
        
        LOGGER.info( String.format("Iteration no. %s", iteration) );
        LOGGER.info( "---------------------------------------------------------------" );

       
        swarm = calculate_performance(swarm, iteration);

        // main loop
        while (iteration<max_iteration_no) { // && !is_qualified(volume, terminate_size)) {
            //choose to keep the current particle position or return to the previous position
            //swarm = updateSwarm(swarm, best_personal_position);
            //find best position
            TEST.info(String.format("iteration %d", iteration));
            best_global_position = find_best_global_position(best_global_position, swarm);
            best_personal_position = find_best_personal_position(best_personal_position, swarm);

            List<Double> performance = performance(swarm);
            DATA.info( String.format("%-20s %-20s %-20s", iteration, performance.get(0), performance.get(1)) );

            //find velocity toward the best position of every particle
            velocity = find_velocity_method_1(swarm, best_personal_position, best_global_position, velocity, iteration);
            //velocity = find_velocity_method_2(swarm, velocity);
            // find new postions of all particles
            swarm = find_new_position(swarm, velocity);
            WEIGHTSET.info(String.format("%-20s %-20s", best_global_position.average_row_cleared, get_best_global_position()));
            WEIGHTSET.info("\n");
            // run a new interation
            iteration++;
            volume = find_volume(swarm);
            swarm = calculate_performance(swarm, iteration);
            //LOGGER.info(String.format("Iteration_no: %d condition: %b %b", iteration, (iteration<max_iteration_no), is_qualified(volume, terminate_size)));
        }

        LOGGER.info( "---------------------------------------------------------------" );
        return;
    }

    private static Double[][] initialize_position(int n, int m) {
        Double[][] result = new Double[n][m];
        for (int i=0; i<n; i++) {
            for (int j=0; j<m; j++) {
                result[i][j] = Math.random();
            }
        }
        return result;
    }

    private static List<Individual> updateSwarm(List<Individual> swarm, List<Individual> best_personal_position) {
        List<Individual> result = new ArrayList<Individual>();
        for (int i=0; i<swarm.size(); i++) {
            if (swarm.get(i).performance<best_personal_position.get(i).performance) {
                result.add(best_personal_position.get(i));
            } else {
                result.add(swarm.get(i));
            }
        }
        return result;
    }

    private static Individual find_best_global_position(Individual best_global_position, List<Individual> swarm) {
        Individual result = new Individual();
        result.position = new ArrayList<Double>();

        result = swarm.get(0);
        for (int i=1; i<swarm.size(); i++) {
            if (result.performance<swarm.get(i).performance) {
                result = swarm.get(i);
            }
        }

        if (best_global_position!=null) {
            if (result.performance<best_global_position.performance) {
                result = best_global_position;
            }
        }

        return result;
    }

    private static List<Individual> find_best_personal_position(List<Individual> best_personal_position,
                                                                List<Individual> swarm) {
        List<Individual> result = new ArrayList<Individual>();

        for (int i=0; i<swarm.size(); i++) {
            if (best_personal_position.get(i).performance<swarm.get(i).performance) {
                result.add(swarm.get(i));
            } else {
                result.add(best_personal_position.get(i));
            }
        }

        return result;
    }

    private static List<List<Double>> find_velocity_method_1(List<Individual> swarm, List<Individual> best_personal_position,
                                                    Individual best_global_position, List<List<Double>> velocity,
                                                    int iteration) {
        List<List<Double>> result = new ArrayList<List<Double>>();
        Double r1 = Math.random();
        Double r2 = Math.random(); 
        
        String interia_value = "";
        String personal_value = "";
        String personal_influence_value = "";
        String global_value = "";
        String global_influence_value = "";
        String next_velocity = "";

        for (int i=0; i<swarm.size(); i++){
            List<Double> particle_velocity = new ArrayList<Double>();   
            //Double jumping_chance = Math.random();
            for (int j=0; j<swarm.get(i).position.size(); j++) {
                /*particle_velocity.add((bestPosition.position.get(j)-swarm.get(i).position.get(j))
                    * (1 - swarm.get(i).performance/bestPosition.performance));*/
                Double new_velocity = 0.0;
                    Double interia = interia_weight * velocity.get(i).get(j);
                    Double personal_influence = 0.0;
                    Double global_influence = 0.0;
                    if (best_personal_position.get(i).performance!=0.0) {
                        personal_influence = cognitive_weight * r1 *
                            // (1+ best_personal_position.get(i).average_row_cleared - swarm.get(i).average_row_cleared)
                            // / (best_global_position.average_row_cleared - swarm.get(i).average_row_cleared + 1) *
                             (best_personal_position.get(i).position.get(j)-swarm.get(i).position.get(j));
                    }
                    personal_influence *= best_personal_position.get(i).performance/best_global_position.performance;
                    //global_influence = (1-swarm.get(i).performance/(100+best_global_position.performance))*
                    global_influence = social_weight * r2 *
                         (best_global_position.position.get(j)-swarm.get(i).position.get(j));

                    

                        new_velocity = interia + personal_influence + global_influence;
                
                    if (i == 13) {
                        interia_value = interia_value + " " + interia;
                        personal_value = personal_value + " " + 
                            (best_personal_position.get(i).position.get(j)-swarm.get(i).position.get(j)); 
                        personal_influence_value += " " + personal_influence;
                        global_value = global_value + " " + 
                            (best_global_position.position.get(j)-swarm.get(i).position.get(j)); 
                        global_influence_value += " " + global_influence;
                        next_velocity = next_velocity + " " + new_velocity;
                    }
                // reduce the velocity when the algorithm is closed to be terminated to increase accuracy
                new_velocity *= (1 - iteration/max_iteration_no);
                // reduce the velocity if the performance is good 
                new_velocity *= (1 - swarm.get(i).performance/best_global_position.performance);
                particle_velocity.add(new_velocity);
                /*particle_velocity.add(velocity.get(i).get(j)
                    + c1*Math.random()*
                        (best_personal_position.get(i).position.get(j)-swarm.get(i).position.get(j))
                    + c1*Math.random()*
                        (best_global_position.position.get(j)-swarm.get(i).position.get(j)));   */
            }
            result.add(particle_velocity);
        }

       // TEST.info(String.format("%20s %20s", "interia_value", interia_value));
       // TEST.info(String.format("%20s %20s", "personal_value", personal_value));
      //  TEST.info(String.format("%20s %20s", "personal_influence_value", personal_influence_value));
       // TEST.info(String.format("%20s %20s", "global_value", global_value));
     //   TEST.info(String.format("%20s %20s", "global_influence_value", global_influence_value));
        //TEST.info(String.format("%20s %20s\n\n", "velocity_value", next_velocity));    
        return result;
    }

    private static List<List<Double>> find_velocity_method_2(List<Individual> swarm, List<List<Double>> velocity) {
        List<List<Double>> result = new ArrayList<List<Double>>();

        /*
        List<List<Double>> distance = new ArrayList<List<Double>>();
        List<List<Double>> acceleration = new ArrayList<List<Double>>();
        for (int i=0; i<SWARM_SIZE; i++) {
            for (int j=0; j<SWARM_SIZE; j++) {
                // if they are two different particles
                (if i!=j) {
                    distance.add(new ArrayList<Double>());
                    for (int k=0; k<WEIGHTSET_SIZE; k++) {
                        distance.get()
                    }
                }
            }
        }*/

        return result;
    }

    private static List<Individual> find_new_position(List<Individual> swarm, List<List<Double>> velocity) {
        List<Individual> result = new ArrayList<Individual>();

        for (int i=0; i<swarm.size(); i++) {
            List<Double> new_position = new ArrayList<Double>();
            for (int j=0; j<swarm.get(i).position.size(); j++) {
                Double position = swarm.get(i).position.get(j)+velocity.get(i).get(j);

                new_position.add(position);
            }
               result.add(new Individual(new_position, i));
        }

        return result;
    }

    private static List<Double> find_volume(List<Individual> swarm) {
        List<Double> result = new ArrayList<Double>();

        for (int i=0; i<swarm.get(0).position.size(); i++) {
            Double max = Double.NEGATIVE_INFINITY;
            Double min = Double.POSITIVE_INFINITY;
            for (int j=0; j<swarm.size(); j++) {
                if (max<swarm.get(j).position.get(i)) {
                    max = swarm.get(j).position.get(i);
                }
                if (min>swarm.get(j).position.get(i)) {
                    min = swarm.get(j).position.get(i);
                }
            }
            result.add(Math.abs(max-min));
        }

        return result;
    }

    private static boolean is_qualified(List<Double> volume, double terminate_size) {
        for (int i=0; i<volume.size(); i++) {
            if (volume.get(i)>terminate_size)
                return false;
        }
        return true;
    }

    private static List<Double> performance(List<Individual> swarm) {
        Double average_row_cleared = 0.0;
        Double average_ratio = 0.0;

        for (Individual particle : swarm) {
            average_row_cleared += particle.average_row_cleared;
            average_ratio += particle.average_ratio;
        }

        average_row_cleared = average_row_cleared / swarm.size();
        average_ratio = average_ratio / swarm.size();

        List<Double> result = new ArrayList<Double>();
        result.add( average_row_cleared );
        result.add( average_ratio );

        return result;
    }

    private static String get_best_global_position() {
        String result = "";
        for (int i=0; i<best_global_position.position.size(); i++) {
            result += " " + Double.toString(best_global_position.position.get(i));
            if (i<best_global_position.position.size()-1) {
                result += ",";
            }
        }

        return result;
    }

    public static void main (String[] args){
        max_iteration_no = Integer.parseInt(args[0]); // 100
        s_and_z_chance = Double.parseDouble(args[1]);
            if (s_and_z_chance>0) {
                is_hard = true;
            } else {
                is_hard = false;
            }
        instance_per_individual = 10; 


        initialize_swarm();

        log_to_screen = true;
        run_pso_algorithm();
    }
}

class Individual {  
    
    public List<Double> position;
    public double average_row_cleared;
    public double average_ratio;
    public double performance;
    public int id;

    public void set_performance(List<Double> result) {
        average_row_cleared = result.get(0);
        average_ratio = result.get(2);
        performance = Individual.performance(average_row_cleared, average_ratio);
    }

    public Individual() {
        position = new ArrayList<Double>();
        id = -1;
    }

    public Individual(List<Double> sequence, int given_id) {
        position = sequence;
        id = given_id;
    }

    // determine the fitness of an individual
    public static Double performance(Double average_row_cleared, Double average_ratio) {
        return ( average_row_cleared / ( 1 + average_ratio - 2.5 ) );
    }
}

class IndividualComparator implements Comparator<Individual> {
    public int compare(Individual first, Individual second) {
        return (int) Math.signum(first.performance - second.performance);
    }
}