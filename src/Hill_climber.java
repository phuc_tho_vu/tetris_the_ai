// to run genetic algorithm

import java.util.*;
import java.text.*;
import java.util.concurrent.*;

public class Hill_climber implements Callable<Integer> {

	public Hill_climber(String instance_name, Double min_row, Integer max_gen, Double s_and_z) {
		String id = instance_name;
		LOGGER = new TetrisLogger("hill_climbing_" + id);
		DATA = new TetrisLogger("performance_" + id);

		// args:
		// min_row_requirement | max_iteration_no | s_and_z_chance | mutation_chance

		min_row_requirement = min_row;
		// Genetic.min_ratio_requirement = Double.parseDouble( args[1] ); // 2.6
		max_iteration_no = max_gen;

		s_and_z_chance = s_and_z;
		// if (s_and_z_chance > 0)
		// 	is_hard = true;
		// else
		// 	is_hard = false;

		GENE_COUNT = 12;
		population_size = 1;
		instance_per_individual = 50;

		initialize_population();
		// LOGGER.info("Initializing population");

		log_to_screen = false;
	}

	// 1 List<Double> = 1 DNA
	private TetrisLogger LOGGER;
  private TetrisLogger DATA;	
	private DecimalFormat df = new DecimalFormat("#.#####");
	private Individual current_cell;

	public boolean log_to_screen = false;
	
	private int GENE_COUNT;
	private int population_size;


	// private static Double multiple_gene_mutation_chance;  
	// private boolean is_hard;
	private Double s_and_z_chance;
	private int instance_per_individual;

	private Double min_row_requirement;
	// private static Double min_ratio_requirement;
	private int max_iteration_no;

	//hill climbing previous
	private int old_feature_to_change;
	private int old_sign;

	// add a few controlled genes + random genes
	public void initialize_population() {

		// some controlled individual
		Double[] controlled_dnas = {0.06213, 0.98444, 0.95405, 0.67058, -0.08458, 0.01187, 0.09450, 0.13067, -0.72974, 0.18073, 0.62325, 0.99738};
		// Double[] controlled_dnas = {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, };

		current_cell = new Individual(Arrays.asList(controlled_dnas), 0);
	}

	// ------------------------------------------------------------------------------

	// return a list of Individuals sorted by increasing fitness
	private Individual live(Individual cell, int iteration_id) {

		String gene_name = String.format("iteration_%s_dna_%s", iteration_id, cell.id);

		DataCollector collector = new DataCollector(gene_name, cell.dna, false, s_and_z_chance, instance_per_individual);
		DataCollector hard_game_collector = new DataCollector(gene_name, cell.dna, true, s_and_z_chance, instance_per_individual);
		cell.set_performance( collector.call(), hard_game_collector.call() );

		return cell;
	}

  // ------------------------------------------------------------------------------

  // select a random feature to change
  private Individual climb(Individual cell, boolean repeat) {
  	
  	double delta = 0.0001;
  	int feature_to_change;
  	int sign;
  	if (repeat) {
  		feature_to_change = old_feature_to_change;
  		sign = old_sign;
  	} else {
	  	feature_to_change = (int)Math.floor(random(0.0, 12.0));
	  	sign = (int)Math.floor(random(0.0, 2.0));
	  }

  	Individual new_cell = new Individual(cell.dna, 1);

  	if (sign == 1)
  		new_cell.dna.set(feature_to_change, cell.dna.get(feature_to_change) + delta);
  	else
  		new_cell.dna.set(feature_to_change, cell.dna.get(feature_to_change) - delta);

  	old_feature_to_change = feature_to_change;
  	old_sign = sign;
  	return new_cell;
  }

	private void hill_climbing(Individual cell) {
		int iteration_id = 0;
    
    DATA.info( String.format("%-20s %-20s %-20s %-20s %-20s", "Iteration", "Avg normal rows", "Normal variance", "Avg hard rows", "Hard variance") );
		// LOGGER.info( String.format("Iteration no. %s", iteration_id) );
		// LOGGER.info( "---------------------------------------------------------------" );

		Individual old_cell = new Individual(cell.dna, iteration_id);
		old_cell.fitness = 0;
		cell = live(cell, iteration_id);

    while (iteration_id < max_iteration_no && !is_qualified(cell)) {

    	boolean repeat = false;
      
      if (cell.fitness > old_cell.fitness) {
      	old_cell = new Individual(cell.dna, cell.id);
      	old_cell.fitness = cell.fitness;
      	LOGGER.info ( String.format("%-20s %-10s N: %-20s NV: %-20s H: %-20s HV: %-20s", "New solution", iteration_id, cell.normal_variance, cell.avg_normal_row_cleared, cell.avg_hard_row_cleared, cell.hard_variance) );
      	DATA.info( "New solution found on iteration: " + Integer.toString(iteration_id) );
      	repeat = true;
      }
			
			LOGGER.info( String.format("Iteration no. %s", iteration_id) );
			LOGGER.info( "---------------------------------------------------------------" );
			// log the genes
			LOGGER.debug( cell.dna );
			DATA.info( String.format("%-20s %-20s %-20s %-20s %-20s", iteration_id, cell.avg_normal_row_cleared, cell.normal_variance, cell.avg_hard_row_cleared, cell.hard_variance) );

      cell = new Individual(old_cell.dna, iteration_id);
      // LOGGER.debug( cell.dna );
      cell = climb(cell, repeat);
			iteration_id++;
      // LOGGER.debug( cell.dna );
      // if (cell.dna == old_cell.dna)
      // 	LOGGER.debug( "Warning similar" );
      // LOGGER.debug( old_cell.dna );

      cell = live(cell, iteration_id);
			// LOGGER.info( String.format("Iteration no. %s", iteration_id) );

			// LOGGER.info( "---------------------------------------------------------------" );
		}

    LOGGER.info( String.format("Iteration no. %s", iteration_id) );
    LOGGER.info( "---------------------------------------------------------------" );
    // log the genes
    LOGGER.debug( cell.dna );
    DATA.info( String.format("%-20s %-20s %-20s", iteration_id, cell.avg_normal_row_cleared, cell.avg_hard_row_cleared) );
	}

	// ------------------------------------------------------------------------------

	public Integer call() {
		hill_climbing(current_cell);
		return 0;
	}

	public static void main(String[] args) {

		// args:
		// min_row_requirement | max_iteration_no | s_and_z_chance

		Double min_row_requirement = Double.parseDouble( args[0] ); // 3000.00
		// // Genetic.min_ratio_requirement = Double.parseDouble( args[1] ); // 2.6
		Integer max_iteration_no = Integer.parseInt  ( args[1] ); // 500

		Double s_and_z_chance = Double.parseDouble( args[2] ); // 0.8
		// if (s_and_z_chance > 0)
		// 	is_hard = true;
		// else
		// 	is_hard = false;

		ExecutorService workers = Executors.newCachedThreadPool();
    List<Hill_climber> tasks = new ArrayList<Hill_climber>();

    // Genetic(String instance_name, Double min_row, Integer max_gen, Double s_and_z, Double wild_chance)

    tasks.add( new Hill_climber("", min_row_requirement, max_iteration_no, s_and_z_chance) );

    try {
      List<Future<Integer>> data = workers.invokeAll(tasks);
      // for (Future<List<Integer>> one_game_data : data) {
      //   List<Integer> game_result = one_game_data.get();
      //   all_game_data.add(game_result);
      // }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    
    workers.shutdown();
    return;

		// // Genetic.multiple_gene_mutation_chance = Double.parseDouble( args[6] ); // 0.1

		// GENE_COUNT = 12;
		// population_size = 64;
		// instance_per_individual = 3;

		// initialize_population();
		// // LOGGER.info("Initializing population");

		// log_to_screen = true;
		// genetic_algorithm(solver_population);

	}

	// ------------------------------------------------------------------------------
	// support functions

	// determine if the population is good (have cleared ~X no of rows, for example)
	private boolean is_qualified(Individual cell) {

		if (cell.avg_normal_row_cleared < min_row_requirement)
			return false;

		return true;
	}

	// random number in [start, end)
	public Double random(Double start, Double end) {
		assert start <= end : "Expecting start <= end for randmization";

		Double random = Math.random();
		Double result = start + ( random * (end - start) );
		return result;
	}
}

class Individual {  
  
  public List<Double> dna;
  public double avg_normal_row_cleared, avg_hard_row_cleared;
  public double normal_variance, hard_variance;
  public double normal_score, hard_score;
  public double fitness;
  public int id;

  public void set_performance(List<Double> normal_gene_result, List<Double> hard_gene_result) {
    avg_normal_row_cleared = normal_gene_result.get(0);
    normal_variance = normal_gene_result.get(3);
    avg_hard_row_cleared = hard_gene_result.get(0);
    hard_variance = hard_gene_result.get(3);

    if (normal_variance == 0 || hard_variance == 0) {
      normal_score = 0;
      hard_score = 0;
    } else {
      normal_score = avg_normal_row_cleared * avg_normal_row_cleared / Math.sqrt(normal_variance);
      hard_score = avg_hard_row_cleared * avg_hard_row_cleared / Math.sqrt(hard_variance);
    }

    fitness = Individual.fitness(normal_score, hard_score);
  }

  public Individual(List<Double> dna_sequence, int given_id) {
    dna = dna_sequence;
    id = given_id;
  }

  // determine the fitness of an individual
  public static Double fitness(Double normal_score, Double hard_score) {
    // return ( avg_normal_row_cleared / ( 1 + avg_normal_ratio - 2.5 ) );
    // the driver is still normal row cleared. then the hard one accounts for 1/5 of that
    // return (normal_score/1000000*1000) * (hard_score/1000*100);
    // max normal row = 1000000 -> scores 1000
    // max hard row = 1000 -> scores 100
    return (normal_score/1000) * (hard_score/10);
  }
}

class IndividualComparator implements Comparator<Individual> {
  public int compare(Individual first, Individual second) {
    return (int) Math.signum(first.fitness - second.fitness);
    // if (first.normal_score < second.normal_score)
    //  return -1;
    // else if (first.normal_score > second.normal_score)
    //  return 1;
    // else {
    //  if (first.hard_score < second.hard_score)
    //    return -1;
    //  else if (first.hard_score > second.hard_score)
    //    return 1;
    //  else return 0;
    // }
  }
}

