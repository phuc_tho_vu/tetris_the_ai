// Shi & Ekerhart's Algorithm
import java.util.*;
import java.lang.Math.*;
import java.text.*;

public class ShiEkerhartPSO {
    private static TetrisLogger LOGGER = new TetrisLogger("ShiEkerhartPSO");
    private static TetrisLogger DATA = new TetrisLogger("performance");
    private static TetrisLogger WEIGHTSET = new TetrisLogger("weightset");
    private static DecimalFormat df = new DecimalFormat("#.#####");
    private static boolean log_to_screen = false;
	private static List<Individual> swarm = new ArrayList<Individual> ();
	private static List<Individual> best_personal_position = new ArrayList<Individual> ();
	private static List<Individual> best_cluster_position = new ArrayList<Individual> ();
	private static Individual best_global_position = new Individual ();
	private static List<List<Double>> velocity = new ArrayList<List<Double>> ();
 	private static List<Double> volume = new ArrayList<Double> ();
 	private static Double interia_weight = 1.0;
 	private static Double cognitive_weight = 1.0;
 	private static Double social_weight = 1.0;
    private static final Integer SWARM_SIZE = 512;
    private static final Integer CLUSTER_NO = 8;
    private static final Integer CLUSTER_SIZE = 64;
    private static final Integer WEIGHTSET_SIZE = 12; 
    private static int min_row_cleared_requirement;
    private static Double min_ratio_requirement;
    private static int max_iteration_no;
    private static Double s_and_z_chance;
    private static boolean is_hard;
    private static int instance_per_individual;

    private static void initialilzation() {
        swarm.clear();
        for (int i=0; i<SWARM_SIZE; i++) {
            List<Double> position = new ArrayList<Double> ();
            for (int j=0; j<WEIGHTSET_SIZE; j++) {
                position.add(Math.random());
            }
            swarm.add(new Individual(position, i));
        }

        best_personal_position = swarm;

        for (int i=0; i<CLUSTER_NO; i++) {
            best_cluster_position.add(swarm.get(i*CLUSTER_SIZE));
        }

        best_global_position = swarm.get(0);

        for (int i=0; i<SWARM_SIZE; i++) {
            List<Double> particle_velocity = new ArrayList<Double>();
            for (int j=0; j<WEIGHTSET_SIZE; j++) {
                particle_velocity.add(0.0);
            }
            velocity.add(particle_velocity);
        }
    }

    // use finest function to evaluate the performance of all particles
    private static void evaluation(int iteration) {
        if (log_to_screen) {
            LOGGER.info( String.format("Iteration: %-20s", iteration) );
            LOGGER.info( String.format("%-30s %-20s %-20s %-20s", "Particle_name", "Avg rows cleared", "Avg ratio", "Performance") );
        } else {
            LOGGER.debug( String.format("Iteration: %-20s", iteration) );
            LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", "Particle_name", "Avg rows cleared", "Avg ratio", "Performance") );
        }

        for (Individual particle : swarm) {
            String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);

            DataCollector collector = new DataCollector(particle_name, particle.position, 
                                                is_hard, s_and_z_chance, instance_per_individual);
            particle.set_performance( collector.call() );
        }

        Collections.sort( swarm, new IndividualComparator());

        for (Individual particle : swarm) {
            if (log_to_screen) {
                String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);
                LOGGER.info( String.format("%-30s %-20s %-20s %-20s", particle_name, df.format(particle.average_row_cleared), 
                    df.format(particle.average_ratio), df.format(particle.performance)) );
            } else {
                String particle_name = String.format("iteration_%s_particle_%s", iteration, particle.id);
                LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", particle_name, df.format(particle.average_row_cleared), 
                    df.format(particle.average_ratio), df.format(particle.performance)) );
            }
        }
    }

    // update best personal positions, best cluster positions, best global position, 
    // velocities and new position of all perticles
    private static void update(int iteration) {
        update_best_personal_positions();
        update_best_cluster_positions();
        update_best_global_position();
        update_velocity(iteration);
        update_swarm_positions();
    }

    private static void update_best_personal_positions() {
        List<Individual> result = new ArrayList<Individual>();

        for (int i=0; i<swarm.size(); i++) {
            if (best_personal_position.get(i).performance<swarm.get(i).performance) {
                result.add(swarm.get(i));
            } else {
                result.add(best_personal_position.get(i));
            }
        }

        best_personal_position = swarm;
    }

    private static void update_best_cluster_positions() {
        List<Individual> result = new ArrayList<Individual> ();

        for (int i=0; i<CLUSTER_NO; i++) {
            Individual temp = best_cluster_position.get(i);
            for (int j=i*CLUSTER_SIZE; j<(i+1)*CLUSTER_SIZE; j++) {
                if (temp.performance < best_personal_position.get(j).performance) {
                    temp = best_personal_position.get(j);
                }
            }
            result.add(temp);
        }

        best_cluster_position = result;
    }

    private static void update_best_global_position() {
        Individual result = best_global_position;

        for (int i=0; i<CLUSTER_NO; i++) {
            if (result.performance<best_cluster_position.get(i).performance) {
                result = best_cluster_position.get(i);
            }
        }

        best_global_position = result;
    }

    private static void update_velocity(int iteration) {
        List<List<Double>> result = new ArrayList<List<Double>>();
        //interia_weight = Math.random();
        cognitive_weight = Math.random();
        social_weight = Math.random();

        for (int i=0; i<swarm.size(); i++){
            int cluster = i/CLUSTER_SIZE;
            List<Double> particle_velocity = new ArrayList<Double>();   
            for (int j=0; j<swarm.get(i).position.size(); j++) {
                /*particle_velocity.add((bestPosition.position.get(j)-swarm.get(i).position.get(j))
                    * (1 - swarm.get(i).performance/bestPosition.performance));*/
                Double new_velocity = 0.0;
                    Double interia = interia_weight * velocity.get(i).get(j);
                    Double personal_influence = 0.0;
                    Double global_influence = 0.0;
                    if (best_personal_position.get(i).performance!=0.0) {
                        personal_influence = cognitive_weight *
                            (best_personal_position.get(i).position.get(j)-swarm.get(i).position.get(j));
                    }
                    personal_influence *= best_personal_position.get(i).performance/best_cluster_position.get(cluster).performance;
                    global_influence = social_weight *
                         (best_cluster_position.get(cluster).position.get(j)-swarm.get(i).position.get(j));

                        new_velocity = interia + personal_influence + global_influence;
                // reduce the velocity when the algorithm is closed to be terminated to increase accuracy
                new_velocity *= (1 - iteration/max_iteration_no);
                // reduce the velocity if the performance is good 
                new_velocity *= (1 - swarm.get(i).performance/best_cluster_position.get(cluster).performance);
                particle_velocity.add(new_velocity);
                /*particle_velocity.add(velocity.get(i).get(j)
                    + c1*Math.random()*
                        (best_personal_position.get(i).position.get(j)-swarm.get(i).position.get(j))
                    + c1*Math.random()*
                        (best_global_position.position.get(j)-swarm.get(i).position.get(j)));   */
            }
            result.add(particle_velocity);
        }

        velocity = result;
    }

    private static void update_swarm_positions() {
        List<Individual> result = new ArrayList<Individual>();

        for (int i=0; i<SWARM_SIZE; i++) {
            List<Double> new_position = new ArrayList<Double> ();
            for (int j=0; j<WEIGHTSET_SIZE; j++) {
                new_position.add(swarm.get(i).position.get(j) + velocity.get(i).get(j));
            }
            result.add(new Individual(new_position, i));
        }

        swarm = result;
    }  

    // use the 1st communication strategy combining with 2nd communication strategy
    private static void communication() {
        for (int i=0; i<CLUSTER_NO; i++) {
            int count = 0;
            int temp_1 = i*CLUSTER_SIZE;
            int temp_2 = temp_1;
            int temp_3 = temp_1;
            // find 3 particles with worst performances in the cluster
            for (int j=i*CLUSTER_SIZE; j<(i+1)*CLUSTER_SIZE; j++) {
                if (swarm.get(temp_1).performance>swarm.get(j).performance) {
                    temp_3 = temp_2;
                    temp_2 = temp_1;
                    temp_1 = j;
                }

                if (swarm.get(temp_2).performance>swarm.get(j).performance 
                    && swarm.get(temp_1).performance<=swarm.get(j).performance
                    && temp_1 != j) {
                    temp_3 = temp_2;
                    temp_2 = j;
                }

                if (swarm.get(temp_3).performance>swarm.get(j).performance 
                    && swarm.get(temp_2).performance<=swarm.get(j).performance
                    && temp_2 != j
                    && temp_1 != j) {
                    temp_3 = j;
                }
            } 
            // remove 3 particles with worst performance with 3 particles
            // one is the global best, two of the cluster best
            swarm.remove(temp_1);
            swarm.remove(temp_2);
            swarm.remove(temp_3);
            swarm.add((i+1)*CLUSTER_SIZE-3, best_global_position);
            swarm.add((i+1)*CLUSTER_SIZE-2, best_cluster_position.get((i+1)%CLUSTER_NO));
            swarm.add((i+1)*CLUSTER_SIZE-1, best_cluster_position.get((i+2)%CLUSTER_NO));    
        }
    }

    private static void ShiEkerhartPSOAlgorithm() {
        int iteration = 0;

        initialilzation();
        DATA.info( String.format("%-20s %-20s %-20s", "Iteration", "Avg rows cleared", "Avg ratio") );
        WEIGHTSET.info(String.format("%-20s %-20s ", "RowsCleared", "weightSet"));

        LOGGER.info( String.format("Iteration no. %s", iteration) );
        LOGGER.info( "---------------------------------------------------------------" );
        while (iteration<max_iteration_no) {
                evaluation(iteration);

            List<Double> performance = performance(swarm);
            DATA.info( String.format("%-20s %-20s %-20s", iteration, performance.get(0), performance.get(1)) );
            
            update(iteration);
            
            if (iteration%15==0 && iteration>0) 
                communication();

            WEIGHTSET.info(String.format("%-20s %-20s", best_global_position.average_row_cleared,
                                                        get_best_global_position()));
            WEIGHTSET.info("\n");

            iteration ++;
        }
        LOGGER.info( "---------------------------------------------------------------" );

    }

    private static List<Double> performance(List<Individual> swarm) {
        Double average_row_cleared = 0.0;
        Double average_ratio = 0.0;

        for (Individual particle : swarm) {
            average_row_cleared += particle.average_row_cleared;
            average_ratio += particle.average_ratio;
        }

        average_row_cleared = average_row_cleared / swarm.size();
        average_ratio = average_ratio / swarm.size();

        List<Double> result = new ArrayList<Double>();
        result.add( average_row_cleared );
        result.add( average_ratio );

        return result;
    }

    private static String get_best_global_position() {
        String result = "";
        for (int i=0; i<best_global_position.position.size(); i++) {
            result += " " + Double.toString(best_global_position.position.get(i));
            if (i<best_global_position.position.size()-1) {
                result += ",";
            }
        }

        return result;
    }

 	public static void main (String[] args) {
        max_iteration_no = Integer.parseInt(args[0]); // 100
        s_and_z_chance = Double.parseDouble(args[1]);
            if (s_and_z_chance>0) {
                is_hard = true;
            } else {
                is_hard = false;
            }
        instance_per_individual = 10; 

        log_to_screen = true;
        ShiEkerhartPSOAlgorithm();    
 	}
}

class Individual {  
    
    public List<Double> position;
    public double average_row_cleared;
    public double average_ratio;
    public double performance;
    public int id;

    public void set_performance(List<Double> result) {
        average_row_cleared = result.get(0);
        average_ratio = result.get(2);
        performance = Individual.performance(average_row_cleared, average_ratio);
    }

    public Individual() {
        position = new ArrayList<Double>();
        id = -1;
    }

    public Individual(List<Double> sequence, int given_id) {
        position = sequence;
        id = given_id;
    }

    // determine the fitness of an individual
    public static Double performance(Double average_row_cleared, Double average_ratio) {
        if (average_ratio != Double.POSITIVE_INFINITY)
            return ( average_row_cleared / ( 1 + average_ratio - 2.5 ) );
        return 0.0;
    }
}

class IndividualComparator implements Comparator<Individual> {
    public int compare(Individual first, Individual second) {
        return (int) Math.signum(first.performance - second.performance);
    }
}
