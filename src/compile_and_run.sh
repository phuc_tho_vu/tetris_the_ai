# #!/bin/bash
username=$1
password=$2
machine=$3

# !/usr/bin/expect
expect <<- DONE
  # modify the path as required

  # ssh to tembusu access node
  spawn ssh $username@tembusu.comp.nus.edu.sg
  # spawn "scp -r ./src $username@tembusu.comp.nus.edu.sg:~/$machine/src "
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"

  # ssh into the compute node
  expect "@access*"
  send -- "ssh -oStrictHostKeyChecking=no $machine\n"

  # go to the source folder for that node
  expect "@*"
  send -- "cd $machine/src\n"

  # clear the logs
  expect "@*"
  send -- "rm ../log/*.*\n"

  # clear the compiled code
  expect "@*"
  send -- "rm *.class\n"

  # re-compile
  expect "@*"
  # local search - Hil climbing
  send -- "javac Hill_climber.java\n"
  # Genetic algo
  # send -- "javac Genetic.java\n"
  # particle swarm algo
  # send -- "javac PSO.java\n"

  # open screen
  expect "@*"
  send -- "screen\n"

  # run code
  expect "@*"
  # local search - Hil climbing
  send -- "java Hill_climber 1000000 1000 0.5\n"
  # genetic algo
  # send -- "java Genetic 1000000 500 0.5 0.2\n"
  # particle swarm
  # send -- "java PSO 10000 0\n"

  # wait 10s before detaching console
  set timeout 60
  send -- "\01\04"

  # exit from the compg terminal
  expect "@*"
  send -- "exit\n"

  # exit from the access terminal
  expect "@access*"
  send -- "exit\n"

  expect eof
DONE