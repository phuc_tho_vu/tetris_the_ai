// implements an instance of the game

import java.util.*;
import java.util.concurrent.*;

public class PlayerSkeleton implements Callable<List<Integer>> {

	// public TetrisLogger LOGGER = new TetrisLogger("tetris");
	private String name; 
	private int algo_id; // indicate whether this PlayerSkeleton use the weights learnt from Genetic - 0, Particle swarm - 1 or Harmonic search - 2
	private List<Double> weights;
	private int[] harmonic_search_weights;
	private boolean have_GUI;
	private boolean is_hard;
	private double hard_pieces_chance;
	private int move_delay;
	private long random_seed;
	public boolean log_to_screen = false;

	// ------------------------------------------------------------------------------

	// creates a game with fully customizable parameters
	public PlayerSkeleton(String instance_name, int algorithm_id, List<Double> feature_weights, boolean show_GUI, boolean more_hard_pieces, double s_and_z_chance, int mili_between_moves, long rand_seed) {
		assert !instance_name.isEmpty() : "Instance does not have a name";

		name = instance_name;
		algo_id = algorithm_id;
		weights = feature_weights;
		have_GUI = show_GUI;
		is_hard = more_hard_pieces;
		hard_pieces_chance = s_and_z_chance;
		move_delay = mili_between_moves;
		random_seed = rand_seed;
	}

	// custom init for harmonic search
	public PlayerSkeleton(String instance_name, int algorithm_id, int[] feature_weights, boolean show_GUI, boolean more_hard_pieces, double s_and_z_chance, int mili_between_moves, long rand_seed) {
		assert !instance_name.isEmpty() : "Instance does not have a name";

		name = instance_name;
		algo_id = algorithm_id;
		harmonic_search_weights = feature_weights;
		have_GUI = show_GUI;
		is_hard = more_hard_pieces;
		hard_pieces_chance = s_and_z_chance;
		move_delay = mili_between_moves;
		random_seed = rand_seed;
	}

	public PlayerSkeleton() {
		Double[] initial_weights = {0.03147592167444934, 0.25287857977527917, 0.28426660170338697, 0.03417471985217327, -0.05350534040471318, -0.02089090531389284, 0.011122287373262853, 0.06943587927070043, 0.07832204699770261, -0.020657616929764144, 0.14212747789046595, 0.2649329851696439};
		
		weights = new ArrayList<Double>( Arrays.asList(initial_weights) );
		move_delay = 3000;
		have_GUI = true;
		random_seed = 191191;
	}

	// pick move using weights learnt from Genetic / PSO
	public int pickMove(State s, int[][] legalMoves, List<Double> weights) {
		int turn_no = s.getTurnNumber();
		int piece_no = s.getNextPiece();
		int result = Features.pickMove(s, legalMoves, weights);
		// LOGGER.info( String.format("Turn %-10s; Piece: %-10s; Move: %-10s", turn_no, piece_no, result) );
		return result;
	}

	// pick move using weights learnt from Harmonic search
	public int pickMove(State s, int[][] legalMoves, int[] weights) {
		int turn_no = s.getTurnNumber();
		int piece_no = s.getNextPiece();
		int result = AlgoByPhuoc_HS.pickMove(s, legalMoves, weights);
		// LOGGER.info( String.format("Turn %-10s; Piece: %-10s; Move: %-10s", turn_no, piece_no, result) );
		return result;
	}

	// ------------------------------------------------------------------------------
	
	public List<Integer> call() {

		// create a easy / hard game
		State game_state;
		if (is_hard) 
			game_state = new State(true, hard_pieces_chance, random_seed);
		else
			game_state = new State(false, 0.0, random_seed); // normal game

		// show GUI or not
		if (have_GUI)
			new TFrame(game_state);

		// main game loop
		while (!game_state.hasLost()) {
			if (algo_id == 0 || algo_id == 1) //genetic or pso
				game_state.makeMove( pickMove(game_state, game_state.legalMoves(), weights) );
			else
				game_state.makeMove( pickMove(game_state, game_state.legalMoves(), harmonic_search_weights) );
			
			if (have_GUI) {
				game_state.draw();
				game_state.drawNext(0,0);
				try {
					Thread.sleep( move_delay );
				} catch (InterruptedException exception) {
					exception.printStackTrace();
				}
			}
		}
		
		// log and return game result
		int row_cleared = game_state.getRowsCleared();
		int piece_used = game_state.getTurnNumber();
		double ratio = (double)piece_used / (double)row_cleared;

		List<Integer> result = new ArrayList<Integer>();
		result.add(row_cleared);
		result.add(piece_used);

		// if (log_to_screen) {
		// 	LOGGER.info( "GAME OVER --------------------------------------------------------" );
		// 	LOGGER.info( String.format("Cleared: %-10s rows. Used %-10s pieces. Avg piece/row: %.5g", row_cleared, piece_used, ratio) );
		// } else {
		// 	LOGGER.debug( "GAME OVER --------------------------------------------------------" );
		// 	LOGGER.debug( String.format("Cleared: %-10s rows. Used %-10s pieces. Avg piece/row: %.5g", row_cleared, piece_used, ratio) );
		// }

		if (log_to_screen) {
			System.out.println( String.format("Cleared: %-10s rows. Used %-10s pieces. Avg piece/row: %.5g", row_cleared, piece_used, ratio) );
		}

		return result;
	}

	// ------------------------------------------------------------------------------

	// if we run this file
	public static void main(String[] args) {
		// PlayerSkeleton game_instance = new PlayerSkeleton();

		// genetic / swarm test
		Double[] initial_weights = {0.0235742677,0.6649619293,0.8565205376,0.7221255211,-0.0534005448,0.0124431806,0.0062603626,0.3741258745,-0.3209849561,0.1005596952,0.4252352874,0.8021023020};
		
		List<Double> starting_weights = new ArrayList<Double>( Arrays.asList(initial_weights) );
		PlayerSkeleton game_instance = new PlayerSkeleton("tetris", 0, starting_weights, true, false, 0.8, 0, 191191);

		// harmonic test
		// int[] initial_weights = {-1026, -1020, -208177856, -32, -20, -31, 25, -6823361, -33, -23, -34380604, -25, 3216};
		// PlayerSkeleton game_instance = new PlayerSkeleton("tetris", 2, initial_weights, true, false, 0.8, 0, 191191);

		game_instance.log_to_screen = true;
		game_instance.call();

		return;
	}
}