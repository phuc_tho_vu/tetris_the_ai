# #!/bin/bash
username=$1
password=$2
machine=$3

# !/usr/bin/expect
expect <<- DONE
  # modify the path as required

  # ssh to tembusu access node
  spawn ssh -oStrictHostKeyChecking=no $username@tembusu.comp.nus.edu.sg
  # spawn "scp -r ./src $username@tembusu.comp.nus.edu.sg:~/$machine/src "
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"

  # delete all source
  expect "access*"
  send -- "yes | rm -r ~/$machine/src\n"

  # exit access node terminal
  set timeout 60
  expect "access*"
  send -- "exit\n"

  # upload code
  spawn scp -r ./src $username@tembusu.comp.nus.edu.sg:~/$machine/src
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"
  expect eof
DONE