import java.util.*;
import java.util.concurrent.*;
import java.io.*;

public class DataCollectorByPhuoc implements Callable<Vector<Integer>> {

	
	
	//<<<<<<< @author Khiem
  public Vector<Integer> call() throws InterruptedException {
    Vector<Integer> result = new Vector<Integer>();    
    // this is a copy of whatever put in Play skeleton
    State s = new State();
    new TFrame(s);
    PlayerSkeleton p = new PlayerSkeleton();
    while(!s.hasLost()) {
      s.makeMove(p.pickMove(s,s.legalMoves()));
      s.draw();
      s.drawNext(0,0);
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }    
    // System.out.println("You have completed "+s.getRowsCleared()+" rows.");
    int row_cleared = s.getRowsCleared();
    int piece_used = s.getTurnNumber();
    float ratio = (float)piece_used*4 / (float)row_cleared;
    
    result.add(row_cleared);
    result.add(piece_used);
    return result;
  }

  // argument: algo_name number_of_instance
  public static void main(String[] args) {
    String algo_name = args[0];
    Integer number_of_instance = 10;
    String data_file_name = "algo";
    if (args.length >= 2)
      number_of_instance = Integer.parseInt(args[1]);


    Vector<Vector<Integer>> algo_running_data = new Vector<Vector<Integer>>();
    // -------------------------------------------------------------------------------------
    ExecutorService workers = Executors.newCachedThreadPool();
    Collection<Callable<Vector<Integer>>> tasks = new Vector<Callable<Vector<Integer>>>();

    for(int i=0; i<number_of_instance; i++) {
      tasks.add(new DataCollector());
    }

    try {
      // List<Future<Vector<Integer>>> results = workers.invokeAll(tasks, 10, TimeUnit.SECONDS);
      List<Future<Vector<Integer>>> results = workers.invokeAll(tasks);
      for (Future<Vector<Integer>> result : results) {
        Vector<Integer> game_result = result.get();
        algo_running_data.add(game_result);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    workers.shutdown();
    // -------------------------------------------------------------------------------------

    try{
      FileWriter file_writer = new FileWriter(algo_name + ".csv");
      BufferedWriter file = new BufferedWriter(file_writer);

      for(Vector<Integer> record : algo_running_data) {
        int row_cleared = record.elementAt(0);
        int piece_used = record.elementAt(1);

        if (row_cleared > 0) {
          float ratio = (float)piece_used*4 / (float)row_cleared;
          System.out.println( String.format("Cleared: %-10s rows. Used %-10s pieces. Avg piece/row: %.5g%n", row_cleared, piece_used, ratio) );
          file.write( String.format("%s,%s,%s\n", row_cleared, piece_used, ratio) );
        } else {
          // System.out.println( String.format("Absolutely did not cleared any row. Used %-10s pieces", piece_used) );
          file.write( String.format("0,%s,0\n", piece_used) );
        }
      }

      file.close();
    } catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
    }

  }
}