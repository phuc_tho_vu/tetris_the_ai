import java.util.*;

public class AlgoByPhuoc_HS {
	public static int NO_OF_ITERATION = 100;
	public static double r_accept = 0.95;
	public static double r_pa = 0.99;
	public static int NO_OF_VAR = 12;
	public static int HMsize = 5;
	public static int[][] HM = new int[HMsize][NO_OF_VAR+1];
	public static double currentBest = 0;
	public static int increasingAmount = 1;
	
	public static void main(String[] args) {
		init();
		int[] tempWeights = new int[12];
		/*for (int i=0; i<12; i++) {
			tempWeights[i] = HM[0][i];
		}*/
		tempWeights = Arrays.copyOfRange(HM[0], 0, 12);
		State s = new State();
		while(!s.hasLost()) {
			PlayerSkeleton p = new PlayerSkeleton();
			s.makeMove(p.pickMove(s, s.legalMoves(), tempWeights));
			//s.draw();
			//s.drawNext(0, 0);
		}
	}

	public static void init() {
		Random random = new Random();
		for (int i=0; i<HMsize; i++) {
			for (int j=0; j<12; j++) {
				HM[i][j] = random.nextInt(100);
			}
			int[] temp = Arrays.copyOfRange(HM[i],0,12);
			HM[i][12] = objectiveFunction(temp);
		}
	}
	
	public static int makeMove(State gameState, int[][] legalMoves) {
		int result = 0;
		
		int[] newWeight = new int[12];
		for (int t=0; t<NO_OF_ITERATION; t++) {
			for (int i=0; i<NO_OF_VAR; i++) {
				double randomDouble = Math.random();
				if (randomDouble < r_accept) {
					//choose a random value from HM for x_i
					int index = (int) (HMsize * Math.random());
					newWeight[i] = HM[index][i];
					if (randomDouble < r_pa) {
						//increase the value (?) by a certain value?
						newWeight[i] = HM[index][i] + increasingAmount;
					}
				} else {
					//choose a random value
					
				}
			}
			//accept new solution if better?
			int averageNewResult = objectiveFunction(newWeight);
			for (int i=0; i<5; i++) {
				if (HM[i][12] < averageNewResult) {
					
				}
			}
		}
		//find current best solution
		//int currentBest = 0;
		for (int i=0; i<HMsize; i++) {
			currentBest = Math.max(currentBest, HM[i][12]);
		}
		return result;
	}
	
	public static int objectiveFunction(int[] weights) {
		int result = 0;
		for (int i=0; i<5; i++) { 		//run 5 games to find average result
			State s = new State();
			PlayerSkeleton p = new PlayerSkeleton();
			while(!s.hasLost()) {
				s.makeMove(p.pickMove(s, s.legalMoves(), weights));
				//s.draw();
				//s.drawNext(0, 0);
			}
			result += s.getRowsCleared();
		}
		result /= 5;
		return result;
	}
	
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	private static int[][] updateNextFieldNoRemove(State s, int[][] legalMoves, int move) {
		int[][] tempField = new int[s.ROWS][s.COLS];
		int currentOrient = legalMoves[move][0];
		int currentSlot = legalMoves[move][1];
		int currentTurn = s.getTurnNumber() + 1;
		int[] currentTop = new int[s.COLS];
		for (int i = 0; i < s.ROWS; i++)
			for (int j = 0; j < s.COLS; j++)
				tempField[i][j] = s.getField()[i][j];

		for (int i = 0; i < s.COLS; i++) {
			currentTop[i] = s.getTop()[i];
		}
		int height = s.getTop()[currentSlot] - s.getpBottom()[s.getNextPiece()][currentOrient][0];
		for (int c = 1; c < s.getpWidth()[s.getNextPiece()][currentOrient]; c++) {
			height = Math.max(height, s.getTop()[currentSlot + c] - s.getpBottom()[s.getNextPiece()][currentOrient][c]);
		}
		if (height + s.getpHeight()[s.getNextPiece()][currentOrient] >= s.ROWS) {
			return null;
		}
		for (int i = 0; i < s.getpWidth()[s.getNextPiece()][currentOrient]; i++) {
			for (int h = height+ s.getpBottom()[s.getNextPiece()][currentOrient][i]; h < height	+ s.getpTop()[s.getNextPiece()][currentOrient][i]; h++) {
				tempField[h][i + currentSlot] = currentTurn;
			}
		}
		for (int c = 0; c < s.getpWidth()[s.getNextPiece()][currentOrient]; c++) {
			currentTop[currentSlot + c] = height
					+ s.getpTop()[s.getNextPiece()][currentOrient][c];
		}
		return tempField;
	}

	private static int[][] updateNextField(State s, int[][] legalMoves, int move) {
		int[][] tempField = new int[s.ROWS][s.COLS];
		int currentOrient = legalMoves[move][0];
		int currentSlot = legalMoves[move][1];
		int currentTurn = s.getTurnNumber() + 1;
		int[] currentTop = new int[s.COLS];

		for (int i = 0; i < s.ROWS; i++)
			for (int j = 0; j < s.COLS; j++)
				tempField[i][j] = s.getField()[i][j];

		for (int i = 0; i < s.COLS; i++) {
			currentTop[i] = s.getTop()[i];
		}

		int height = s.getTop()[currentSlot] - s.getpBottom()[s.getNextPiece()][currentOrient][0];
		for (int c = 1; c < s.getpWidth()[s.getNextPiece()][currentOrient]; c++) {
			height = Math.max(height, s.getTop()[currentSlot + c] - s.getpBottom()[s.getNextPiece()][currentOrient][c]);
		}
		if (height + s.getpHeight()[s.getNextPiece()][currentOrient] >= s.ROWS) {
			return null;
		}
		for (int i = 0; i < s.getpWidth()[s.getNextPiece()][currentOrient]; i++) {
			for (int h = height+ s.getpBottom()[s.getNextPiece()][currentOrient][i]; h < height	+ s.getpTop()[s.getNextPiece()][currentOrient][i]; h++) {
				tempField[h][i + currentSlot] = currentTurn;
			}
		}

		for (int c = 0; c < s.getpWidth()[s.getNextPiece()][currentOrient]; c++) {
			currentTop[currentSlot + c] = height + s.getpTop()[s.getNextPiece()][currentOrient][c];
		}

		/*check for full rows - starting at the top, check all columns in the row*/
		/*if the row was full - remove it and slide above stuff down, for each column, slide down all bricks*/
		int removedLine = 0;
		for (int r = height + s.getpHeight()[s.getNextPiece()][currentOrient] - 1; r >= height; r--) {
			boolean full = true;
			for (int c = 0; c < s.COLS; c++) {
				if (tempField[r][c] == 0) {
					full = false;
					break;
				}
			}
			if (full) {
				for (int c = 0; c < s.COLS; c++) {
					for (int i = r; i < currentTop[c]; i++) {
						tempField[i][c] = tempField[i + 1][c];
					}
				}
			}
		}
		return tempField;
	}

	public static void printField(int[][] field) {
		System.out.println("========================");
		for (int i=19; i>=0; i--) {
			for (int j=0; j<10; j++) {
				if (field[i][j] == 0) {
					System.out.print("  *");
				} else {
					System.out.printf("%3d",field[i][j]);
				}
			}
			System.out.println();
		}
		System.out.println("========================");
	}
	public static int[] top(int[][] nextField) {
		//printField(nextField);
		int[] top = new int[10];
		for (int i = 0; i < 10; i++) {
			top[i] = 0;
			for (int j = 19; j >= 0; j--) {
				if (nextField[j][i] != 0 && top[i] < j)
					top[i] = j + 1;
			}
		}
		return top;
	}

	/*1, 2, 3, 4, 5, 6 by Trang*/
	//feature 1
	public static int pileHeight(int[] top) {
		List<Integer> myList = new ArrayList<Integer>();
		for (int i = 0; i < top.length; i++) {
			myList.add(top[i]);

		}
		Collections.sort(myList);
		return myList.get(myList.size() - 1);
	}

	//feature 2
	public static int holes(int[][] field, int[] top) {
		int numHoles = 0;
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if (field[i][j] == 0 && i < top[j]) {
					numHoles += 1;
				}
			}
		}
		return numHoles;
	}

	//feature 3
	public static int connectedHoles(int[][] field,int[] top){
		int holes = 0;
		for(int col= 0; col< field[0].length; col++){
			holes += getConnectedHolesByCol(field,col,top);

		}
		return holes;
	}

	public static int getConnectedHolesByCol(int[][] field,int col,int[] top) {
		int holes = 0;
		int height = top[col];
		int index = 0;
			while (index < top[col]) {
				if (field[index][col] != 0) {
					index++;
				} else {
					holes +=1;
					while (index < top[col] && field[index][col] ==0) {
						index++;
					}
				}
			}
		return holes;
	}

	//feature 4
	public static int removedLine (int[][] nextField, int[][] tempNextField) {
		int result;
		result = top(tempNextField)[0] - top(nextField)[0];
		return result;
	}
	//feature 5
	public static int altitudeDifference(int[] top) {
		List<Integer> myList = new ArrayList<Integer>();
		for (int i= 0; i< top.length; i++) {
			myList.add(top[i]);
		}
		Collections.sort(myList);
		return (myList.get(myList.size()-1) - myList.get(0));
	}

	public static List<Integer> getWellDepth(int[][] nextField, int rows,
			int cols, int[] top) {

		List<Integer> wellDepth = new ArrayList<Integer>();

		// near the edge
		if (top[0] < top[1]) {
			wellDepth.add(top[1] - top[0]);

		}

		if (top[cols - 2] > top[cols - 1]) {
			wellDepth.add(top[cols - 2] - top[cols - 1]);
		}

		for (int i = 1; i < cols - 1; i++) {
			if (top[i - 1] > top[i] && top[i] < top[i + 1]) {
				int lowerHeight = top[i - 1];
				if (top[i - 1] > top[i +1])
					lowerHeight = top[i +1];
				wellDepth.add(lowerHeight - top[i]);
			}
		}
		return wellDepth;
	}

	//feature 6
	public static int maxWellDepth(List<Integer> wells) {
		int maxWellDepth = 0;
		for (int i=0; i<wells.size(); i++) {
			maxWellDepth = Math.max(maxWellDepth, wells.get(i));
		}
		return maxWellDepth;
	}

	//feature 7
	public static double sumOfAllWells(List<Integer> wells) {
				double sum = 0;
		for (int i = 0; i < wells.size(); i++) {
			sum += wells.get(i);
		}
		return sum;
	}

	//feature 8
	public static int landingHeight(int[][] nextField, int landingHeight) {
		int result = landingHeight;
		return result;
	}

	//feature 9
	public static int blocks(int[][] nextField) {
		int result = 0;
		for (int i=0; i<nextField.length; i++) {
			for (int j=0; j<nextField[0].length; j++) {
				if (nextField[i][j] != 0) {
					result++;
				}
			}
		}
		return result;
	}

	//feature 10
	public static int weightedBlocks(int[][] nextField, int[] top) {
		int weightedBlock = 0;
		for (int i=0; i<10; i++) {
			for (int j=top[i]-1; j>=0; j--) {
				if (nextField[j][i]!=0) {
					weightedBlock += j+1;
				}
			}
		}
		return weightedBlock;
	}

	//feature 11
	public static int rowTransition(int[][] nextField, int[] top) {
		int result = 0;
		int maxTop = top[0];
		for (int i=0; i<10; i++) {
				maxTop = Math.max(maxTop, top[i]);
		}
		maxTop = 19;
		for (int i=0; i<maxTop; i++) {
			if (nextField[i][0] == 0) {
				result++;
			}
			if (nextField[i][9] == 0) {
				result++;
			}
			for (int j=0; j<8; j++) {
				if ((nextField[i][j]!=0 && nextField[i][j+1]==0) || (nextField[i][j]==0 && nextField[i][j+1]!=0)) {
					result++;
				}
			}
		}
		return result;
	}

	//feature 12
	public static int colTransition(int[][] nextField, int[] top) {
		int result = 0;
		int maxTop = top[0];
		for (int i=0; i<10; i++) {
			if (maxTop < top[i]) {
				maxTop = top[i];
			}
		}
		maxTop = 19;
		for (int j=0; j<10; j++) {
			if (nextField[0][j] == 0) {
				result++;
			}
			if (nextField[maxTop][j] != 0) {
				result++;
			}
			if (maxTop>1) {
				for (int i=0; i<maxTop-1; i++) {
					if ((nextField[i][j]!=0 && nextField[i+1][j]==0) || (nextField[i][j]==0 && nextField[i+1][j]!=0)) {
						result++;
					}
				}
			}
		}
		return result;
	}

	//pickMove, included weights
	public static int pickMove(State gameState, int[][] legalMoves, int[] weights) {
		int[][] nextField = new int[20][10];
		int[][] tempNextField = new int[20][10];

		double weight = Double.NEGATIVE_INFINITY;
		int action = 0;
		for (int i = 0; i < legalMoves.length; i++) {
			nextField = updateNextField(gameState, legalMoves, i);
			tempNextField = updateNextFieldNoRemove(gameState, legalMoves, i);
			int landingHeight = 0;
			if (nextField != null) {
				for (int j=0; j<gameState.getpWidth()[gameState.getNextPiece()][legalMoves[i][0]];j++) {
					landingHeight = Math.max(landingHeight, top(nextField)[legalMoves[i][1]+j]);
				}
				double thisWeight = evaluateState(nextField, tempNextField, gameState.ROWS, gameState.COLS, landingHeight, weights);
				if (weight < thisWeight) {
					weight = thisWeight;
					action = i;
				}
			} else {
				weight = Double.NEGATIVE_INFINITY;
			}
		}
		return action;	
	}
	
	//evaluate states, included weights
	private static double evaluateState(int[][] nextField, int[][] tempNextField, int rows,
			int cols, int landingHeight, int[] weights) {
		double stateWeight = 0;
		int[] top = new int[cols];
		for (int i = 0; i < cols; i++) {
			for (int j = rows - 1; j >= 0; j--) {
				if (nextField[j][i] != 0 && top[i] < j)
					top[i] = j + 1;
			}
		}

		List<Integer> wells = getWellDepth(nextField, rows, cols, top);
		Collections.sort(wells);
		//int[] w = {2382,-135028,-48491,-74343, 59739,-72528,-61591, 13344,-19737,-47653,-50015,-144688};
		//int[] w = {-62709, -30271, 0, 0, 0, -12, 0, 0, 0, 0, 0, 0};
		double[] e = {0.935, 1.839, 0.905, 1.078, 0.858, 2.207, 1.565, 0.023, 0.117, 0.894, 1.474, 1.346};
		int[] w = {-62709, -30271, 0, -48621, 35395, -12, -43810, 0, 0, -4041, -44262, -5832};
		double pileHeight         = weights[0] * pileHeight(top);
		double holes              = weights[1] * holes(nextField, top);
		double connectHoles       = weights[2] * connectedHoles(nextField, top);
		double removedLine 	      = weights[3] * removedLine(nextField, tempNextField);
		double altitudeDifference = weights[4] * altitudeDifference(top);
		double maxWellDepth       = weights[5] * maxWellDepth(wells);
		double sumOfAllWells      = weights[6] * sumOfAllWells(wells);
		double newLandingHeight   = weights[7] * landingHeight;
		double blocks             = weights[8] * blocks(nextField);
		double weightedBlock      = weights[9] * weightedBlocks(nextField, top);
		double rowTransition      = weights[10] * rowTransition(nextField, top);
		double colTransition      = weights[11] * colTransition(nextField, top);

		stateWeight += pileHeight + holes + connectHoles + removedLine + altitudeDifference + maxWellDepth +
						sumOfAllWells + newLandingHeight + blocks + weightedBlock + rowTransition + colTransition;
		return stateWeight;
	}
}