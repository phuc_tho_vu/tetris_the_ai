import re
from decimal import Decimal
import math

# soc_nodes = [ 'access0' ]
soc_nodes = [ 'compg31', 'compg32', 'compg33', 'compg34', 'compg35', 'compg36', 'compg37', 'compg38', 'compg39', 'compg40', 'compg41', 'compg42', 'compg43', 'compg44', 'compg45', 'compg46', 'compg47', 'compg48', 'compg49', 'compg50', 'compg51', 'compg52', 'compg53', 'access0', 'access1', 'access2', 'access3', 'sma0', 'sma1', 'sma2', 'sma3', 'sma4', 'sma5', 'sma6', 'sma7', 'sma8' ]

log_files = []
for machine in soc_nodes:
  file_name = './data/' + machine + '/genetic_algo_1.log';
  log_files.append(file_name)

# test = '@DEBUG      generation_11_dna_90           0.42                 1.5236               0.22                 0.3716              '
# test = '@DEBUG      {0.004711378423990498, -0.2819835499308254, -0.9492997828514422, -0.46018564569806664, 0.028522656016233272, 6.342292732428589E-4, -0.2436773733036357, 0.06799662645208748, -0.87606099436832, 0.6084435487909132, 0.4009507049688026, -0.20253793781670648, },'
gene_data_format = r'@DEBUG      {([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), ([+-]?[0-9].[0-9]*[eE]*[-+]*[0-9]*), },\s'
performance_data_format = r'@DEBUG      generation_[0-9]+_dna_[0-9]+\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s'
# matches = re.search( performance_data_format, test )
# if matches:
#   print (matches.groups())

selected_gene = []
selected_data = []
for file_name in log_files:
  log = open ( file_name, 'r' )
  lines = log.readlines()
  log.close()

  gene_data = []
  performance_data = []
  count = 0
  for data in lines:
    count += 1
    gene_matches = re.search( gene_data_format, data )
    performance_matches = re.search( performance_data_format, data )

    if gene_matches:
      gene_data.append( gene_matches.groups() )
    elif performance_matches:
      performance_data.append( performance_matches.groups() )
    # else:
    #  print( '|' + data + '||' )

  gene_data = gene_data[0:len(gene_data)-64]
  # print (len(gene_data))
  # print (len(performance_data))
  # print (gene_data[-1])
  # print (performance_data[-1])

  for index in range(0, len(performance_data)):
    performance = performance_data[index]
    avg_normal_rows = Decimal(performance[0])
    avg_hard_rows = Decimal(performance[2])
    # if avg_normal_rows > 50000 and avg_hard_rows > 100:
    # print ( str(avg_normal_rows) + ' | ' + str(avg_hard_rows) )
    if avg_normal_rows > 50000.0:
      # print ( index )
      # print ( gene_data[index] )
      selected_gene.append(gene_data[index])
      selected_data.append(performance_data[index])


output = open( './data/selected.csv', 'w' )
output.write( '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}\n'.format('avg_normal_rows', 'normal_standard_deviation', 'avg_hard_rows', 'hard_standard_deviation', 'max_column_height_penalty', 'column_height_penalty', 'hole_count_penalty', 'weighted_hole_penalty', 'row_cleared_reward', 'altitude_difference_penalty', 'max_well_depth_penalty', 'well_depth_penalty', 'cell_count_penalty', 'weighted_cell_count_penalty', 'row_transition_penalty', 'column_transition_penalty') )

for gene in selected_gene:
  performance = selected_data[selected_gene.index(gene)]
  output.write( '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}\n'.format( performance[0], math.sqrt(float(performance[1])), performance[2], math.sqrt(float(performance[3])), gene[0], gene[1], gene[2], gene[3], gene[4], gene[5], gene[6], gene[7], gene[8], gene[9], gene[10], gene[11]) )

output.close()