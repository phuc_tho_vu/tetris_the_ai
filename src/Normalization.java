import java.util.*;

public class Normalization {
	public static void main (String[] agrs) {
		Double[] result = new Double[12];

		for (int i=0; i<12; i++) {
			result[i] = Double.parseDouble(agrs[i]);
		}

		Double max = result[0];

		for (int i=1; i<12; i++) {
			if (max<result[i]) {
				max = result[i];
			}
		}

		for (int i=0; i<12; i++) {
			result[i] = result[i]/max;
		}

		for (int i=0; i<12; i++) {
			System.out.println(result[i]);
		}
	}

}