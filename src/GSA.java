import java.util.*;
import java.lang.Math.*;
import java.text.*;

public class GSA {
	// logging
	private static TetrisLogger LOGGER = new TetrisLogger("GSA");
    private static TetrisLogger DATA = new TetrisLogger("GSAperformance");
    private static DecimalFormat df = new DecimalFormat("#.#####");
    // constant
    private static final int POPULATION_SIZE = 32;
    private static final int WEIGHTSET_SIZE = 12;
    // variables
  	private static List<Individual>  population = new ArrayList<Individual>();
  	private static List<List<Double>> velocity = new ArrayList<List<Double>>();
  	private static List<List<Double>> acceleration = new ArrayList<List<Double>>();
  	private static Individual best_position = new Individual();
  	private static int max_iteration_no = 100; // default values
  	private static Double s_and_z_chance = 0.0; // default values
  	private static boolean is_hard = false;
  	private static boolean log_to_screen = false;
  	private static int instance_per_individual = 10;

    public static void run_GSA_algorithm() {
    	int iteration = 0;

    	DATA.info( String.format("%-20s %-20s %-20s", "Iteration", "Avg rows cleared", "Avg ratio") );
        LOGGER.info( String.format("Iteration no. %s", iteration) );
        LOGGER.info( "---------------------------------------------------------------" );

    	initialization();
    	while (iteration<max_iteration_no) {
    		population = evaluate_performance(population, iteration);
    		update(population, velocity, acceleration);

    		iteration++;
    	}
    }

	private static void initialization() {
  		for (int i=0; i<POPULATION_SIZE; i++) {
  			List<Double> position = new ArrayList<Double>();
  			for (int j=0; j<WEIGHTSET_SIZE; j++) {
  				position.add(Math.random());
  			}
  			population.add(new Individual(position, i));
  		}

  		for (int i=0; i<POPULATION_SIZE; i++) {
  			velocity.add(new ArrayList<Double>());
  			acceleration.add(new ArrayList<Double>());
  			for (int j=0; j<WEIGHTSET_SIZE; j++) {
  				velocity.get(i).add(0.0);
  				acceleration.get(i).add(0.0);
  			}
  		}
  	}

  	private static List<Individual> evaluate_performance (List<Individual> population, int iteration) {
  		if (log_to_screen) {
            LOGGER.info( String.format("Iteration: %-20s", iteration) );
            LOGGER.info( String.format("%-30s %-20s %-20s %-20s", "Mass_name", "Avg rows cleared", "Avg ratio", "Performance") );
        } else {
            LOGGER.debug( String.format("Iteration: %-20s", iteration) );
            LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", "Mass_name", "Avg rows cleared", "Avg ratio", "Performance") );
        }

        for (Individual mass : population) {
            String mass_name = String.format("iteration_%s_particle_%s", iteration, mass.id);

            DataCollector collector = new DataCollector(mass_name, mass.position, 
                                                is_hard, s_and_z_chance, instance_per_individual);
            mass.set_performance( collector.call() );
        }

        Collections.sort( population, new IndividualComparator() );


        for (Individual mass : population) {
            if (log_to_screen) {
                String mass_name = String.format("iteration_%s_particle_%s", iteration, mass.id);
                LOGGER.info( String.format("%-30s %-20s %-20s %-20s", mass_name, df.format(mass.average_row_cleared), 
                    df.format(mass.average_ratio), df.format(mass.performance)) );
            } else {
                String mass_name = String.format("iteration_%s_particle_%s", iteration, mass.id);
                LOGGER.debug( String.format("%-30s %-20s %-20s %-20s", mass_name, df.format(mass.average_row_cleared), 
                    df.format(mass.average_ratio), df.format(mass.performance)) );
            }
        }
        return population;
  	}

  	private static void update(List<Individual> population, List<List<Double>> velocity,
  									List<List<Double>> acceleration) {
  		best_position = find_best_position(population);
  		acceleration = update_acceleration(population, best_position);
  		velocity = update_velocity(velocity, acceleration);
  		population = update_position(population, velocity);
  	}

  	private static Individual find_best_position(List<Individual> population) {
  		Individual result = new Individual();
        result.position = new ArrayList<Double>();

        result = population.get(0);
        for (int i=1; i<population.size(); i++) {
            if (result.performance<population.get(i).performance) {
                result = population.get(i);
            }
        }

        if (best_position!=null) {
            if (result.performance<best_position.performance) {
                result = best_position;
            }
        }

        return result;
    }

  	private static List<List<Double>> update_acceleration (List<Individual> population,
  																Individual best_position) {
  		List<List<Double>> result = new ArrayList<List<Double>>();
  		List<Double> mass = new ArrayList<Double>();
  		List<List<Double>> distance = new ArrayList<List<Double>>();
  		Double total_mass = 0.0;
  		Double best = best_position.performance;
  		Double worst = 0.0;

  		for (int i=0; i<POPULATION_SIZE + 1; i++) {
  			distance.add(new ArrayList<Double>());
  			for (int j=0; j<POPULATION_SIZE + 1; j++) {
  				distance.get(i).add(0.0);
  			}
  		}

  		// find best and worst performance
  		for (int i=0; i<POPULATION_SIZE; i++) {
  		  	if (worst > population.get(i).performance) 
  				worst = population.get(i).performance;
  		}

  		// calculate masses
  		for (int i=0; i<POPULATION_SIZE; i++) {
  			total_mass += (population.get(i).performance - worst)/(best - worst);
  		}
  		total_mass += best_position.performance;

  		for (int i=0; i<POPULATION_SIZE + 1; i++) {
  			Double temp =0.0;
  			if (i!= POPULATION_SIZE) {
  				temp = (population.get(i).performance - worst)/(best - worst);
  			} else {
  				temp = (best_position.performance - worst)/(best - worst);
  			}
  			mass.add(temp/total_mass);
  		}

  		// calculate distances 
  		for (int i=0; i<POPULATION_SIZE + 1; i++) {
  			for (int j=i+1; j<POPULATION_SIZE + 1; j++) {
  				Double _distance = 0.0;
  				for (int k=0; k<WEIGHTSET_SIZE; k++) {
  					if (j!=POPULATION_SIZE) {
  						_distance += Math.pow(population.get(i).position.get(k) - population.get(j).position.get(k),2);
  					} else {
  						_distance += Math.pow(population.get(i).position.get(k) - best_position.position.get(k),2);
  					}
  				}
  				_distance = Math.sqrt(_distance);
  				distance.get(i).set(j, _distance);
  				distance.get(j).set(i, _distance);
  			}
  		}

  		for (int i=0; i<POPULATION_SIZE; i++) {
  			List<Double> particle_acceleration = new ArrayList<Double>();
  			for (int j=0; j<WEIGHTSET_SIZE; j++) {
  				Double temp =0.0;
  				for (int k=0; k<POPULATION_SIZE + 1; k++) {
  					if (i!=k) {
  						if (k!=POPULATION_SIZE) {
  							temp += (mass.get(k)/(1+Math.pow(distance.get(i).get(k),2))) *
				  						(population.get(k).position.get(j)-population.get(i).position.get(j));
  						} else {
  							temp += (mass.get(k)/(1+Math.pow(distance.get(i).get(k),2))) *
				  						(best_position.position.get(j)-population.get(i).position.get(j));
  						} 
  					}
  				}
  				particle_acceleration.add(temp);
  			}
  			result.add(particle_acceleration);
  		}

  		return result;
  	}

  	private static List<List<Double>> update_velocity (List<List<Double>> velocity,
  														List<List<Double>> acceleration) {
  		List<List<Double>> result = new ArrayList<List<Double>>();

  		for (int i=0; i<POPULATION_SIZE; i++) {
  			List<Double> new_velocity = new ArrayList<Double>();
  			for (int j=0; j<WEIGHTSET_SIZE; j++) {
  				new_velocity.add(velocity.get(i).get(j) + acceleration.get(i).get(j));
  			}
  			result.add(new_velocity);	
  		}

  		return result;
  	}

  	private static List<Individual> update_position (List<Individual> population,
  														List<List<Double>> velocity) {
  		List<Individual> result = new ArrayList<Individual>();

  		for (int i=0; i<POPULATION_SIZE; i++) {
  			List<Double> position = new ArrayList<Double>();
  			for (int j=0; j<WEIGHTSET_SIZE; j++) {
  				position.add(population.get(i).position.get(j) + velocity.get(i).get(j));
  			}
  			result.add(new Individual(position, i));
  		}

  		return result;
  	}

	public static void main (String[] args) {
		max_iteration_no = Integer.parseInt(args[0]); // 100
        s_and_z_chance = Double.parseDouble(args[1]);
            if (s_and_z_chance>0) {
                is_hard = true;
            } else {
                is_hard = false;
            }
        instance_per_individual = 10; 
        log_to_screen = true;

		run_GSA_algorithm();
	}
}

class Individual {  
    
    public List<Double> position;
    public double average_row_cleared;
    public double average_ratio;
    public double performance =0.0;
    public int id; 

    public void set_performance(List<Double> result) {
        average_row_cleared = result.get(0);
        average_ratio = result.get(2);
        performance = Individual.performance(average_row_cleared, average_ratio);
    }

    public Individual() {
        position = new ArrayList<Double>();
        id = -1;
    }

    public Individual(List<Double> sequence, int given_id) {
        position = sequence;
        id = given_id;
    }

    // determine the fitness of an individual
    public static Double performance(Double average_row_cleared, Double average_ratio) {
        return ( average_row_cleared / ( 1 + average_ratio - 2.5 ) );
    }
}

class IndividualComparator implements Comparator<Individual> {
    public int compare(Individual first, Individual second) {
        return (int) Math.signum(first.performance - second.performance);
    }
}