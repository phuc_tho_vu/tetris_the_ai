// to run genetic algorithm

import java.util.*;
import java.text.*;
import java.util.concurrent.*;

public class Genetic implements Callable<Integer> {

  public Genetic(String instance_name, Double min_row, Integer max_gen, Double s_and_z, Double wild_chance) {
    String id = instance_name;
    LOGGER = new TetrisLogger("genetic_algo_" + id);
    DATA = new TetrisLogger("genetic_performance_" + id);

    // args:
    // min_row_requirement | max_generation_no | s_and_z_chance | mutation_chance

    min_row_requirement = min_row;
    // Genetic.min_ratio_requirement = Double.parseDouble( args[1] ); // 2.6
    max_generation_no = max_gen;

    s_and_z_chance = s_and_z;
    // if (s_and_z_chance > 0)
    //   is_hard = true;
    // else
    //   is_hard = false;

    // Genetic.selection_cutoff_threshold  = Double.parseDouble( args[4] ); // 0.5
    mutation_chance = wild_chance;
    // Genetic.multiple_gene_mutation_chance = Double.parseDouble( args[6] ); // 0.1

    GENE_COUNT = 12;
    population_size = 128;
    instance_per_individual = 50;

    initialize_population();
    // LOGGER.info("Initializing population");

    log_to_screen = false;
  }

  // 1 List<Double> = 1 DNA
  private TetrisLogger LOGGER;
  private static TetrisLogger DATA = new TetrisLogger("performance");
  private DecimalFormat df = new DecimalFormat("#.#####");
  private List<Individual> solver_population = new ArrayList<Individual>();

  public boolean log_to_screen = false;
  
  private int GENE_COUNT;
  private int population_size;


  // private static Double selection_cutoff_threshold;
  private Double mutation_chance;
  // private static Double multiple_gene_mutation_chance;  
  // private boolean is_hard;
  private Double s_and_z_chance;
  private int instance_per_individual;

  private Double min_row_requirement;
  // private static Double min_ratio_requirement;
  private int max_generation_no;

  // add a few controlled genes + random genes
  public void initialize_population() {

    // some controlled individual
    Double[][] controlled_dnas = {
      // max_column_height_penalty - column_height_penalty - hole_count_penalty - weighted_hole_penalty
      // row_cleared_reward - altitude_difference_penalty - max_well_depth_penalty - well_depth_penalty
      // cell_count_penalty - weighted_cell_count_penalty
      // row_transition_penalty - column_transition_penalty
    };

    for (int i = 0; i < controlled_dnas.length; i++) {
      solver_population.add( new Individual(Arrays.asList(controlled_dnas[i]), i) );
    }

    // random individuals
    int random_individual_count = population_size - solver_population.size();
    for (int i = 0; i < random_individual_count; i++) {
      Double[] dna = new Double[GENE_COUNT];
      for (int j = 0; j < GENE_COUNT; j++) {
        dna[j] = Math.random();
      }      
      solver_population.add( new Individual(Arrays.asList(dna), i + controlled_dnas.length) );
    }

    // initialize mutation pool
    // mutation_pool.setSeed(System.currentTimeMillis());
  }

  // ------------------------------------------------------------------------------

  // return a list of Individuals sorted by increasing fitness
  private List<Individual> live(List<Individual> population, int generation_id) {

    if (log_to_screen) {
      // LOGGER.info( String.format("Generation: %-10s", generation_id) );
      LOGGER.info( String.format("%-30s %-20s %-20s %-20s %-20s", "Gene_name", "Avg normal rows", "Normal variance", "Avg hard rows", "Hard variance") );
    } else {
      // LOGGER.debug( String.format("Generation: %-10s", generation_id) );
      LOGGER.debug( String.format("%-30s %-20s %-20s %-20s %-20s", "Gene_name", "Avg normal rows", "Normal variance", "Avg hard rows", "Hard variance") );
    }

    for (Individual cell : population) {
      String gene_name = String.format("generation_%s_dna_%s", generation_id, cell.id);

      DataCollector collector = new DataCollector(gene_name, cell.dna, false, s_and_z_chance, instance_per_individual);
      DataCollector hard_game_collector = new DataCollector(gene_name, cell.dna, true, s_and_z_chance, instance_per_individual);
      cell.set_performance( collector.call(), hard_game_collector.call() );
    }
    Collections.sort( population, new IndividualComparator() );

    for (Individual cell : population)
      if (log_to_screen) {
        String gene_name = String.format("generation_%s_dna_%s", generation_id, cell.id);
        LOGGER.info( String.format("%-30s %-20s %-20s %-20s %-20s", gene_name, df.format(cell.avg_normal_row_cleared), df.format(cell.normal_variance), df.format(cell.avg_hard_row_cleared), df.format(cell.hard_variance)) );
      } else {
        String gene_name = String.format("generation_%s_dna_%s", generation_id, cell.id);
        LOGGER.debug( String.format("%-30s %-20s %-20s %-20s %-20s", gene_name, df.format(cell.avg_normal_row_cleared), df.format(cell.normal_variance), df.format(cell.avg_hard_row_cleared), df.format(cell.hard_variance)) );
      }

    return population;
  }

  // ------------------------------------------------------------------------------

  // i.e: within the top X% individuals, select one with uniform rate
  public int select_uniform_with_cutoff(List<Individual> population) {
    Double selection_cutoff_threshold = 0.5;
    int result = (int)((Math.random()*(1 - selection_cutoff_threshold) + selection_cutoff_threshold)*population.size());
    return result;
  }

  // i.e: each individual have increase selection chance based on their fitness
  public int fitness_proportionate_selection(List<Individual> population) {
    Double total_fitness = 0.0;
    for ( Individual cell:population ) {
      // LOGGER.info( String.format("fitness: [N:%s, NV:%s, H:%s, HV:%s, NS:%s, HS:%s, F:%s]", cell.avg_normal_row_cleared, cell.normal_variance, cell.avg_hard_row_cleared, cell.hard_variance, cell.normal_score, cell.hard_score, cell.fitness) );
      total_fitness += cell.fitness;
    }

    Double rand = random(0.0, 1.0); // [0, 1)
    // LOGGER.info( String.format("rand: %s", rand) );
    Double lower_bound = 0.0;
    Double upper_bound = 0.0;
    // LOGGER.info( String.format("bound: [%s - %s]", lower_bound, upper_bound) );
    // LOGGER.info( String.format("total_fitness: %s", total_fitness) );
    for (int i = 0; i < population.size(); i++) {
      Individual cell = population.get(i);
      upper_bound += cell.fitness / total_fitness;

      // LOGGER.info( String.format("bound: [%s - %s]", lower_bound, upper_bound) );
      if (lower_bound <= rand && rand < upper_bound) {//found 
        // LOGGER.info( String.format("return: %s", i) );
        return i;
      }
      lower_bound = upper_bound;
    }

    assert 1 == 0 : "Roulette could not select any individual";
    LOGGER.info( "Roulette could not select any individual" ); //not found, alert
    return 0;
  }

  // choose k (the tournament size) individuals from the population at random
  // choose the best individual from pool/tournament with probability p
  // choose the second best individual with probability p*(1-p)
  // choose the third best individual with probability p*((1-p)^2)
  // and so on...
  // public static int tournament_selection(List<Individual> population) {
  //   int number_of_tournament_participants = 2; // k
  //   Double select_chance = 0.4; // p

  //   List<Individual> participants = new ArrayList<Individual>();
  //   for (int i=0; i < number_of_tournament_participants; i++) {
  //     int participant_index = (int)( random(0.0, 1.0) * population.size() );
  //     participants.add( population.get(participant_index) );
  //   }

  //   Collections.sort( participants, new IndividualComparator() );
  //   Double rand = random(0.0, 1.0); // [0, 1)
  //   Double lower_bound = 0.0;
  //   Double upper_bound = 0.0;
  //   for ( Individual cell:participants ) {
  //     Double chance = select_chance;
  //     for (int i = number_of_tournament_participants; i > 1; i--)
  //       chance = chance * (1 - select_chance);
  //     upper_bound += chance;
      
  //     if (lower_bound <= rand && rand < upper_bound) //found
  //       return population.indexOf(cell);
  //     lower_bound = upper_bound;
  //   }

  //   // throw new Exception();
  //   // assert false : "Tournament could not select any individual"; //not found, alert
  //   // return 0;
  // }

  public int random_selection(List<Individual> population) {
    // for ( Individual cell : population ) {
    //   LOGGER.info( String.format("Candidate: %-20s $-20s", cell.dna.get(0), cell.dna.get(1)) );
    // }

    int index = fitness_proportionate_selection(population);
    // DATA.info( String.format("Selected: %-20s", index) );

    return index;
  }

  // ------------------------------------------------------------------------------

  // each gene have a 1/no_of_genes_in_dna chance to mutate
  private List<Double> bit_string_mutation(List<Double> original_dna, Double gene_mutation_chance) {
    List<Double> mutated_dna = new ArrayList<Double>();

    for (int i=0; i<original_dna.size(); i++) {
      Double rand = random(0.0, 1.0);
      if (rand < gene_mutation_chance) {
        mutated_dna.add( random(-1.0, 1.0) );
      } else {
        mutated_dna.add( original_dna.get(i) );
      }
    }

    return mutated_dna;
  }

  private List<Double> mutate(List<Double> original_dna) {
    Double rand = random(0.0, 1.0);

    // DATA.info( "-----------------\n" );
    // DATA.info( original_dna );

    if (rand < mutation_chance) { //mutate
      List<Double> mutated_dna;
      // mutated_dna = bit_string_mutation(original_dna, 1.0 / (double)original_dna.size());
      mutated_dna = bit_string_mutation(original_dna, 0.4);
      // DATA.info( "Mutate to\n" );
      // DATA.info( mutated_dna );
      return  mutated_dna;
    } else
      // DATA.info( original_dna );
      return original_dna;
  }

  // ------------------------------------------------------------------------------

  // Mom:      1-2-3-4-5-6-7-8
  // Dad:      A-B-C-D-E-F-G-H
  // Slice:           |
  // Child1:  1-2-3-4-E-F-G-H
  // Child2:  A-B-C-D-5-6-7-8
  // Slice the genes into 2 sequence and exchange
  private List<List<Double>> one_point_cross_over(Individual mom, Individual dad) {
    List<List<Double>> children = new ArrayList<List<Double>>();
    int split_index = (int)( Math.random()*mom.dna.size() );

    // DATA.info( "---------------------------\n" );
    // DATA.info( mom.dna );
    // DATA.info( dad.dna );
    // DATA.info( "\n" );

    List<Double> child_1_mom_genes = mom.dna.subList( 0, split_index );
    List<Double> child_1_dad_genes = dad.dna.subList( split_index, dad.dna.size() );
    List<Double> child_1 = new ArrayList<Double>();
    child_1.addAll(child_1_mom_genes);
    child_1.addAll(child_1_dad_genes);
    // DATA.info( child_1 );

    List<Double> child_2_mom_genes = mom.dna.subList( split_index, mom.dna.size() );
    List<Double> child_2_dad_genes = dad.dna.subList( 0, split_index );
    List<Double> child_2 = new ArrayList<Double>();
    child_2.addAll(child_2_dad_genes);
    child_2.addAll(child_2_mom_genes);
    // DATA.info( child_2 );

    children.add(child_1);
    children.add(child_2);

    // DATA.info( "---------------------------\n" );

    return children;
  }

  // ------------------------------------------------------------------------------
  // Mom:      1-2-3-4-5-6-7-8
  // Dad:      A-B-C-D-E-F-G-H
  //
  // Child1:  1-B-C-4-5-6-G-8
  // Child2:  A-2-3-D-E-F-7-H
  // break adn down to genes. Each gene have ~50% chance being exchanged
  private List<List<Double>> uniform_cross_over(Individual mom, Individual dad) {
    List<List<Double>> children = new ArrayList<List<Double>>();
    List<Double> child_1 = new ArrayList<Double>();
    List<Double> child_2 = new ArrayList<Double>();

    for (int i=0; i<dad.dna.size(); i++) {
      Double rand = random(0.0, 1.0);
      if (rand < 0.5) {
        child_1.add( dad.dna.get(i) );
        child_2.add( mom.dna.get(i) );
      } else {
        child_1.add( mom.dna.get(i) );
        child_2.add( dad.dna.get(i) );
      }
    }

    // DATA.info( child_1 );
    // DATA.info( child_2 );

    children.add(child_1);
    children.add(child_2);

    // DATA.info( "---------------------------\n" );

    return children;
  }


  // cross breeding between mom & dad, create 2 child dna
  private List<List<Double>> breed(Individual mom, Individual dad) {
    return uniform_cross_over(mom, dad);
  }

  private List<Double> performance(List<Individual> population) {
    Double avg_normal_row_cleared = 0.0;
    Double avg_hard_row_cleared = 0.0;

    for (Individual cell : population) {
      avg_normal_row_cleared += cell.avg_normal_row_cleared;
      avg_hard_row_cleared += cell.avg_hard_row_cleared;
    }

    avg_normal_row_cleared = avg_normal_row_cleared / population.size();
    avg_hard_row_cleared = avg_hard_row_cleared / population.size();

    List<Double> result = new ArrayList<Double>();
    result.add( avg_normal_row_cleared );
    result.add( avg_hard_row_cleared );

    return result;
  }

  // ------------------------------------------------------------------------------

  private void genetic_algorithm(List<Individual> population) {
    int generation_id = 0;
      DATA.info( String.format("%-20s %-20s %-20s", "Generation", "Avg rows cleared", "Avg ratio") );
    
    DATA.info( String.format("%-20s %-20s %-20s", "Generation", "Avg normal rows", "Avg hard rows") );
    LOGGER.info( String.format("Generation no. %s", generation_id) );
    LOGGER.info( "---------------------------------------------------------------" );

    // log the genes
      for ( Individual cell : population ) {
        LOGGER.debug( cell.dna );
      }

    population = live(population, generation_id);

    while (generation_id < max_generation_no && !is_qualified(population)) {

      List<Double> performance = performance(population);
      DATA.info( String.format("%-10s %-10s %-10s", generation_id, performance.get(0), performance.get(1)) );

      List<Individual> new_population = new ArrayList<Individual>();      
      int half_size = (int)(population.size()/2);
      
      for (int i = 0; i < half_size; i++) {
        int mom_index = random_selection(population);
        int  dad_index = random_selection(population);
        while ( dad_index == mom_index ) {
          // LOGGER.info ( String.format("Dad: %s | Mom %s", dad_index, mom_index) );
          dad_index = random_selection(population); //repick to get different individuals
        }

        List<List<Double>> children = breed( population.get(mom_index), population.get(dad_index) );
        Individual child_1 = new Individual( mutate(children.get(0)), i);
        Individual child_2 = new Individual( mutate(children.get(1)), i + half_size);
        new_population.add(child_1);
        new_population.add(child_2);
      }

      // for (Individual cell : new_population) {
      //   DATA.info( String.format("%-20s %-20s", cell.dna.get(0), cell.dna.get(1)) );
      // }

      generation_id++;

      LOGGER.info( String.format("Generation no. %s", generation_id) );
      LOGGER.info( "---------------------------------------------------------------" );
      // log the genes
      for ( Individual cell : population ) {
        LOGGER.debug( cell.dna );
      }

      population = live(new_population, generation_id);
      // LOGGER.info( String.format("Generation no. %s", generation_id) );

      // LOGGER.info( "---------------------------------------------------------------" );

    }

    for ( Individual cell : population ) {
      LOGGER.debug( cell.dna );
    }
  }

  // ------------------------------------------------------------------------------

  public Integer call() {
    genetic_algorithm(solver_population);
    return 0;
  }

  public static void main(String[] args) {

    // args:
    // min_row_requirement | max_generation_no | s_and_z_chance | mutation_chance

    Double min_row_requirement = Double.parseDouble( args[0] ); // 3000.00
    // // Genetic.min_ratio_requirement = Double.parseDouble( args[1] ); // 2.6
    Integer max_generation_no = Integer.parseInt  ( args[1] ); // 500

    Double s_and_z_chance = Double.parseDouble( args[2] ); // 0.8
    // if (s_and_z_chance > 0)
    //   is_hard = true;
    // else
    //   is_hard = false;

    // // Genetic.selection_cutoff_threshold  = Double.parseDouble( args[4] ); // 0.5
    Double mutation_chance = Double.parseDouble( args[3] ); // 0.2

    ExecutorService workers = Executors.newCachedThreadPool();
    List<Genetic> tasks = new ArrayList<Genetic>();
    
    // Genetic(String instance_name, Double min_row, Integer max_gen, Double s_and_z, Double wild_chance)

    // for (int i = 0; i < 2; i++) {
    //   String algo_id = Integer.toString(i);
    tasks.add( new Genetic("1", min_row_requirement, max_generation_no, s_and_z_chance, mutation_chance) );
    // }

    try {
      List<Future<Integer>> data = workers.invokeAll(tasks);
      // for (Future<List<Integer>> one_game_data : data) {
      //   List<Integer> game_result = one_game_data.get();
      //   all_game_data.add(game_result);
      // }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    
    workers.shutdown();
    return;

    // // Genetic.multiple_gene_mutation_chance = Double.parseDouble( args[6] ); // 0.1

    // GENE_COUNT = 12;
    // population_size = 64;
    // instance_per_individual = 3;

    // initialize_population();
    // // LOGGER.info("Initializing population");

    // log_to_screen = true;
    // genetic_algorithm(solver_population);

  }

  // ------------------------------------------------------------------------------
  // support functions

  // determine if the population is good (have cleared ~X no of rows, for example)
  private boolean is_qualified(List<Individual> population) {

    for (Individual cell : population) {
      if (cell.avg_normal_row_cleared < min_row_requirement)
        return false;
    }

    return true;
  }

  // random number in [start, end)
  public Double random(Double start, Double end) {
    assert start <= end : "Expecting start <= end for randmization";

    Double random = Math.random();
    Double result = start + ( random * (end - start) );
    return result;
  }
}

class Individual {  
  
  public List<Double> dna;
  public double avg_normal_row_cleared, avg_hard_row_cleared;
  public double normal_variance, hard_variance;
  public double normal_score, hard_score;
  public double fitness;
  public int id;

  public void set_performance(List<Double> normal_gene_result, List<Double> hard_gene_result) {
    avg_normal_row_cleared = normal_gene_result.get(0);
    normal_variance = normal_gene_result.get(3);
    avg_hard_row_cleared = hard_gene_result.get(0);
    hard_variance = hard_gene_result.get(3);

    if (normal_variance == 0 || hard_variance == 0) {
      normal_score = 0;
      hard_score = 0;
    } else {
      normal_score = avg_normal_row_cleared * avg_normal_row_cleared / Math.sqrt(normal_variance);
      hard_score = avg_hard_row_cleared * avg_hard_row_cleared / Math.sqrt(hard_variance);
    }

    fitness = Individual.fitness(normal_score, hard_score);
  }

  public Individual(List<Double> dna_sequence, int given_id) {
    dna = dna_sequence;
    id = given_id;
  }

  // determine the fitness of an individual
  public static Double fitness(Double normal_score, Double hard_score) {
    // return ( avg_normal_row_cleared / ( 1 + avg_normal_ratio - 2.5 ) );
    // the driver is still normal row cleared. then the hard one accounts for 1/5 of that
    // return (normal_score/1000000*1000) * (hard_score/1000*100);
    // max normal row = 1000000 -> scores 1000
    // max hard row = 1000 -> scores 100
    return (normal_score/1000) * (hard_score/10);
  }
}

class IndividualComparator implements Comparator<Individual> {
  public int compare(Individual first, Individual second) {
    return (int) Math.signum(first.fitness - second.fitness);
    // if (first.normal_score < second.normal_score)
    //   return -1;
    // else if (first.normal_score > second.normal_score)
    //   return 1;
    // else {
    //   if (first.hard_score < second.hard_score)
    //     return -1;
    //   else if (first.hard_score > second.hard_score)
    //     return 1;
    //   else return 0;
    // }
  }
}
