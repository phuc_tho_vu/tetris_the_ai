// to run genetic algorithm

import java.util.*;
import java.text.*;
import java.util.concurrent.*;
import java.nio.charset.*;
import java.io.*;


public class Tournament implements Callable<List<List<Integer>>> {
  // private List<List<Double>> genetic_and_swarm_weights = new ArrayList<List<Double>>(); // weights for genetic and swarm
  // private List<List<Integer>> harmonic_weights = new ArrayList<List<Integer>>();
  // private long random_seed;
  ExecutorService workers;
  List<PlayerSkeleton> tasks;

  public Tournament(String tournament_id, List<List<Double>> genetic_and_swarm_weights, List<List<Integer>> harmonic_weights, boolean is_hard_game, double s_and_z_chance, long seed) {
    
    workers = Executors.newCachedThreadPool();
    tasks = new ArrayList<PlayerSkeleton>();

    int weight_count = 0;

    // create all games using weights from genetic & swarm
    for (List<Double> weights : genetic_and_swarm_weights) {
      String game_name =  tournament_id + "_w" + Integer.toString(weight_count);
      
      if (is_hard_game) {
        tasks.add( new PlayerSkeleton(game_name, 0, weights, false, true, s_and_z_chance, 0, seed) );
      } else {
        tasks.add( new PlayerSkeleton(game_name, 0, weights, false, false, 0, 0, seed) );
      }

      weight_count++;
    }

    // create all game using harmonic search weights
    for (List<Integer> weights : harmonic_weights) {
      String game_name =  tournament_id + "_w" + Integer.toString(weight_count);
      Integer[] tmp_array = weights.toArray(new Integer[0]);
      int[] weight_array = new int[weights.size()];
      int i=0;
      for( Integer num : tmp_array ) {
        weight_array[i++] = num;
      }
      
      if (is_hard_game) {
        tasks.add( new PlayerSkeleton(game_name, 2, weight_array, false, true, s_and_z_chance, 0, seed) );
      } else {
        tasks.add( new PlayerSkeleton(game_name, 2, weight_array, false, false, 0, 0, seed) );
      }

      weight_count++;      
    }
  }

  public List<List<Integer>> call() {
    List<List<Integer>> all_game_data = new ArrayList<List<Integer>>();

    try {
      List<Future<List<Integer>>> data = workers.invokeAll(tasks);
      for (Future<List<Integer>> one_game_data : data) {
        List<Integer> game_result = one_game_data.get();
        all_game_data.add(game_result); // 0: row cleared - 1: piece used
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    
    workers.shutdown();
    
    return all_game_data;
  }

  public static void main(String[] args) {
    //read all the weights and consttruct a List<List<Double>> and a List<List<Integer>>
    List<List<Double>> genetic_and_swarm_weights = new ArrayList<List<Double>>();
    List<List<Integer>> harmonic_weights = new ArrayList<List<Integer>>();
    List<WeightSet> candidates = new ArrayList<WeightSet>();
    TetrisLogger LOGGER = new TetrisLogger("tournament");

    //genetic weights
    List<String> lines;
    int weight_set_count = 0;
    try {
      InputStream    fis;
      BufferedReader br;
      String         line;

      fis = new FileInputStream("genetic_weights");
      br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
      while ((line = br.readLine()) != null) {
        List<Double> one_weight_set = new ArrayList<Double>();
        String[] parts = line.split(",");
        
        for(String part : parts) {
          // System.out.println(part);
          one_weight_set.add( Double.parseDouble(part) );
        }
        
        candidates.add( new WeightSet(weight_set_count++) );
        genetic_and_swarm_weights.add( one_weight_set );
      }

      // Done with the file
      br.close();
      br = null;
      fis = null;

      // ----------------------------------------------------

      fis = new FileInputStream("pso_weights");
      br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
      while ((line = br.readLine()) != null) {
        List<Double> one_weight_set = new ArrayList<Double>();
        String[] parts = line.split(",");
        
        for(String part : parts) {
          // System.out.println(part);
          one_weight_set.add( Double.parseDouble(part) );
        }

        candidates.add( new WeightSet(weight_set_count++) );
        genetic_and_swarm_weights.add( one_weight_set );
      }

      // Done with the file
      br.close();
      br = null;
      fis = null;

      // ----------------------------------------------------

      fis = new FileInputStream("harmonic_weights");
      br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
      while ((line = br.readLine()) != null) {
        List<Integer> one_weight_set = new ArrayList<Integer>();
        String[] parts = line.split(",");
        
        for(String part : parts) {
          // System.out.println(part);
          one_weight_set.add( Integer.parseInt(part) );
          
        }

        candidates.add( new WeightSet(weight_set_count++) );
        harmonic_weights.add( one_weight_set );
      }

      // Done with the file
      br.close();
      br = null;
      fis = null;

    } catch (Exception exception) {
      exception.printStackTrace();
    }


    // -----------------------------------------------------------------------
    weight_set_count = 0;
    TetrisLogger WEIGHTS = new TetrisLogger("tournament_weights");
    WEIGHTS.debug( String.format("%-10s %s", "ID", "Weights") );
    for( List<Double> set : genetic_and_swarm_weights) {
      String weight_set = "Weight set:\n";
      for(Double x : set) {
        weight_set += String.format("%s, ", x);
      }
      candidates.get(weight_set_count).weights = weight_set;
      WEIGHTS.debug( String.format("%-10s %s", weight_set_count, weight_set) );
      weight_set_count++;
    }

    for( List<Integer> set : harmonic_weights) {
      String weight_set = "Weight set:\n";
      for(Integer x : set) {
        weight_set += String.format("%s, ", x);
      }
      candidates.get(weight_set_count).weights = weight_set;
      WEIGHTS.debug( String.format("%-10s %s", weight_set_count, weight_set) );
      weight_set_count++;
    }

    // -----------------------------------------------------------------------
    
    Random generator = new Random();
    Double s_and_z_chance = 0.5;


    int tournament_count = 100; // 100
    
    // normal game
    int current_tournament_id = 0;
    for( int i=0; i<tournament_count; i++) {
      long tournament_seed = generator.nextLong();
      String tournament_name = "n" + String.format("%s", current_tournament_id++);
      LOGGER.info( "Tournament: " + tournament_name );
      
      Tournament the_test = new Tournament(tournament_name, genetic_and_swarm_weights, harmonic_weights, false, 0.0, tournament_seed);
      // System.out.println("Task no");
      // System.out.println(the_test.tasks.size());
      
      List<List<Integer>> all_results = the_test.call();
      // System.out.println("Result no");
      // System.out.println(all_results.size());

      int result_count = 0;
      for( List<Integer> one_result : all_results) {
        candidates.get(result_count++).add_normal_game_result( one_result );
      }
      // System.out.println("Result count");
      // System.out.println(result_count);
    }


    // hard game
    current_tournament_id = 0;
    for( int i=0; i<tournament_count; i++) {
      long tournament_seed = generator.nextLong();
      String tournament_name = "h" + String.format("%s", current_tournament_id++);
      LOGGER.info( "Tournament: " + tournament_name );
      
      Tournament the_test = new Tournament(tournament_name, genetic_and_swarm_weights, harmonic_weights, true, s_and_z_chance, tournament_seed);
      
      List<List<Integer>> all_results = the_test.call();

      int result_count = 0;
      for( List<Integer> one_result : all_results) {
        candidates.get(result_count++).add_hard_game_result( one_result );
      }
    }

    // -----------------------------------------------------------------------

    DecimalFormat df = new DecimalFormat("#.#####");
    TetrisLogger DATA = new TetrisLogger("tournament_data");
    DATA.info( String.format("%-5s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s",
      "ID", "Norm.Mean", "Norm.Std.dev", "Norm.Cof.variation",
      "Norm.Min", "Norm.Q1", "Norm.Q3", "Norm.Max", "Norm.Ratio",
      "Hard.Mean", "Hard.Std.dev", "Hard.Cof.variation",
      "Hard.Min", "Hard.Q1", "Hard.Q3", "Hard.Max", "Hard.Ratio") );
    for( WeightSet w : candidates ) {
      w.calculate_performance();
      DATA.info( String.format("%-5s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s",
      w.id, df.format(w.normal_mean), df.format(w.normal_standard_deviation), df.format(w.normal_coefficient_of_variation),
      df.format(w.normal_min), df.format(w.normal_q1), df.format(w.normal_q3), df.format(w.normal_max), df.format(w.normal_piece_ratio_mean),
      df.format(w.hard_mean), df.format(w.hard_standard_deviation), df.format(w.hard_coefficient_of_variation),
      df.format(w.hard_min), df.format(w.hard_q1), df.format(w.hard_q3), df.format(w.hard_max), df.format(w.hard_piece_ratio_mean)) );
    }
  }
}

class WeightSet {
  List<List<Integer>> normal_results = new ArrayList<List<Integer>>();
  List<List<Integer>> hard_results = new ArrayList<List<Integer>>();
  public String weights = "";
  int id;
  // row cleared
  public Double normal_mean = 0.0, hard_mean = 0.0;
  public Double normal_standard_deviation = 0.0, hard_standard_deviation = 0.0;
  public Double normal_coefficient_of_variation = 0.0, hard_coefficient_of_variation = 0.0;
  public Double normal_min = 0.0, hard_min = 0.0;
  public Double normal_q1 = 0.0, hard_q1 = 0.0;
  public Double normal_q3 = 0.0, hard_q3 = 0.0;
  public Double normal_max = 0.0, hard_max = 0.0;
  // piece used
  public Double normal_piece_ratio_mean = 0.0, hard_piece_ratio_mean = 0.0;

  public WeightSet(int the_id) {
    id = the_id;
  }

  public void add_normal_game_result(List<Integer> game_result) {
    normal_results.add( new ArrayList<Integer>(game_result) );
    // System.out.println("Add 1 result");
  }

  public void add_hard_game_result(List<Integer> game_result) {
    hard_results.add( game_result );
  }

  public void calculate_performance() {
    for( List<Integer> one_result : normal_results ) {
      normal_mean += one_result.get(0); // row cleared
      normal_piece_ratio_mean += one_result.get(1); // piece used
    }

    // System.out.println("Sum of row cleared");
    // System.out.println(normal_mean);

    // System.out.println("Numer of normal_results");
    // System.out.println(normal_results.size());

    normal_mean = normal_mean / (double) normal_results.size();
    // System.out.println("normal_mean");
    // System.out.println(normal_mean);
    normal_piece_ratio_mean = normal_piece_ratio_mean / (double) normal_results.size();
    normal_piece_ratio_mean = normal_piece_ratio_mean / normal_mean;

    for( List<Integer> one_result : normal_results ) {
      normal_standard_deviation += (one_result.get(0) - normal_mean) * (one_result.get(0) - normal_mean);
    }
    normal_standard_deviation = normal_standard_deviation / (double) normal_results.size();
    normal_standard_deviation = Math.sqrt( normal_standard_deviation );
    normal_coefficient_of_variation = normal_standard_deviation / normal_mean;
    normal_min = normal_mean - normal_standard_deviation;
    normal_q1 = normal_mean - 0.67 * normal_standard_deviation;
    normal_q3 = normal_mean + 0.68 * normal_standard_deviation;
    normal_max = normal_mean + normal_standard_deviation;



    for( List<Integer> one_result : hard_results ) {
      hard_mean += one_result.get(0); // row cleared
      hard_piece_ratio_mean += one_result.get(1); // piece used
    }

    hard_mean = hard_mean / (double) hard_results.size();
    hard_piece_ratio_mean = hard_piece_ratio_mean / (double) hard_results.size();
    hard_piece_ratio_mean = hard_piece_ratio_mean / hard_mean;

    for( List<Integer> one_result : hard_results ) {
      hard_standard_deviation += (one_result.get(0) - hard_mean) * (one_result.get(0) - hard_mean);
    }
    hard_standard_deviation = hard_standard_deviation / (double) hard_results.size();
    hard_standard_deviation = Math.sqrt( hard_standard_deviation );
    hard_coefficient_of_variation = hard_standard_deviation / hard_mean;
    hard_min = hard_mean - hard_standard_deviation;
    hard_q1 = hard_mean - 0.67 * hard_standard_deviation;
    hard_q3 = hard_mean + 0.68 * hard_standard_deviation;
    hard_max = hard_mean + hard_standard_deviation;
  }

}