# #!/bin/bash
username=$1
password=$2
machine=$3
output_folder=$4


# !/usr/bin/expect
expect <<- DONE
  # modify the path as required

  spawn scp -r $username@tembusu.comp.nus.edu.sg:~/$machine/log ./$output_folder/$machine
  expect "*?assword:*" # will be prompted for password
  send -- "$password\n"

  set timeout 4000

  expect eof
DONE