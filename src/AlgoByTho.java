import java.util.*;

class AlgoByTho {

	public static int pickMove(State gameState, int[][] legalMoves, int method) {
		int[][] nextField = new int[gameState.ROWS][gameState.COLS]; // field == game grid
		double weight = -1000000000;
		int action = 0;
		/* for each legal move of this state, create the corresponding next
		 * state's field, evaluate the weight of the next state choose action
		 * base on the weight of the next state */
		for (int m = 0; m < legalMoves.length; m++) {
			nextField = updateNextField(gameState, legalMoves, m);
			if (nextField != null) {
				double thisWeight = evaluateState(nextField, gameState.ROWS,
						gameState.COLS, method);
				// System.out.printf("%d\n", thisWeight);
				if (weight <= thisWeight) {
					weight = thisWeight;
					action = m;
				}
			} else {
				return 0;
			}
		}
		return action;
	}

	// whole code taken from State.makeMove(int, int)
	private static int[][] updateNextField(State s, int[][] legalMoves, int move) {
		int[][] tempField = new int[s.ROWS][s.COLS];
		int curOrient = legalMoves[move][0];
		int curSlot = legalMoves[move][1];
		int curTurn = s.getTurnNumber() + 1;
		int[] curTop = new int[s.COLS];

		// update the current state's field and top
		for (int i = 0; i < s.ROWS; i++)
			for (int j = 0; j < s.COLS; j++)
				tempField[i][j] = s.getField()[i][j];

		for (int i = 0; i < s.COLS; i++) {
			curTop[i] = s.getTop()[i];
		}

		// height if the first column makes contact, for each column beyond the first in the piece, check if game ends
		int height = s.getTop()[curSlot] - s.getpBottom()[s.getNextPiece()][curOrient][0]; 
		for (int c = 1; c < s.getpWidth()[s.getNextPiece()][curOrient]; c++) {
			height = Math.max(height, s.getTop()[curSlot + c] - s.getpBottom()[s.getNextPiece()][curOrient][c]);
		}
		if (height + s.getpHeight()[s.getNextPiece()][curOrient] >= s.ROWS) {
			return null;
		}
		// for each column in the piece - fill in the appropriate blocks, from bottom to top of brick
		for (int i = 0; i < s.getpWidth()[s.getNextPiece()][curOrient]; i++) {
			for (int h = height+ s.getpBottom()[s.getNextPiece()][curOrient][i]; h < height	+ s.getpTop()[s.getNextPiece()][curOrient][i]; h++) {
				tempField[h][i + curSlot] = curTurn;
			}
		}

		// adjust top
		for (int c = 0; c < s.getpWidth()[s.getNextPiece()][curOrient]; c++) {
			curTop[curSlot + c] = height
					+ s.getpTop()[s.getNextPiece()][curOrient][c];
		}

		/*check for full rows - starting at the top, check all columns in the row*/
		/*if the row was full - remove it and slide above stuff down, for each column, slide down all bricks*/
		for (int r = height + s.getpHeight()[s.getNextPiece()][curOrient] - 1; r >= height; r--) {
			boolean full = true;
			for (int c = 0; c < s.COLS; c++) {
				if (tempField[r][c] == 0) {
					full = false;
					break;
				}
			}
			if (full) {
				for (int c = 0; c < s.COLS; c++) {
					for (int i = r; i < curTop[c]; i++) {
						tempField[i][c] = tempField[i + 1][c];
					}
				}
			}
		}
		return tempField;
	}

	private static double evaluateState(int[][] nextField, int rows, int cols, int method) {
		double stateWeight = 0;
		int[] top = new int[cols];
		// printState(nextField, rows, cols);
		for (int i = 0; i < cols; i++) {
			for (int j = rows - 1; j >= 0; j--) {
				if (nextField[j][i] != 0 && top[i] < j)
					top[i] = j + 1;
			}
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				stateWeight += evaluateBlock1(nextField, i, j, rows, cols, top);

			}
		}

		List<Integer> wells = getWellDepth(nextField, rows, cols, top);
		Collections.sort(wells);

		stateWeight -= wells.get(wells.size() - 1);
		stateWeight -= getSumOfAllWells(wells);

		return stateWeight;
	}

	private static double evaluateBlock1(int[][] nextField, int i, int j, int rows, int cols, int[] top) {
		double blockWeight = 0;

		int maxTop = 0;
		for (int c = 0; c < cols; c++) {
			maxTop = Math.max(top[c], maxTop);
		}

		// get penalty for height
		double heightPenalty = 0;
		if (nextField[i][j] != 0) {
			if (i < 4) {
				heightPenalty = (-20) * Math.pow(i + 1, 1);
			} else if (i < 8) {
				heightPenalty = (-20) * Math.pow(i + 1, 1);
			} else {
				heightPenalty = (-40) * Math.pow(i + 1, 1);
			}
			int highestHole = 0;
			for (int c = 0; c < i; c++) {
				if (nextField[c][j] == 0) { // have block any unoccupied block
					highestHole = c;
				}
			}
			// heightPenalty -= (int)Math.pow(i-highestHole, 4);
		} else {
			if (i < top[j])
				heightPenalty = (-10) * Math.pow((i + 1), 4);
		}

		// get penalty for surface pattern
		double patternPenalty = 0;
		if (nextField[i][j] != 0) {
			if (i == top[j] - 1) { // on surface
				if (j != 0) { // not right-most column
					if (Math.abs(top[j - 1] - i - 1) > 2
							&& Math.abs(top[j - 1] - i - 1) < 4) {
						patternPenalty -= 20 * Math.pow(top[j - 1] - i - 1, 2);
					} else if (Math.abs(top[j - 1] - i - 1) >= 4) {
						patternPenalty -= 40 * Math.pow(
								Math.abs(top[j - 1] - i - 1), 4);
					}
				}

				if (j != 9) { // not left-most column
					if (Math.abs(top[j + 1] - i - 1) > 2
							&& Math.abs(top[j + 1] - i - 1) < 4) {
						patternPenalty -= 20 * Math.pow(top[j + 1] - i - 1, 2);
					} else if (Math.abs(top[j + 1] - i - 1) >= 4) {
						patternPenalty -= 40 * Math.pow(
								Math.abs(top[j + 1] - i - 1), 4);
					}
				}
			}
		}

		blockWeight += heightPenalty + patternPenalty;

		return blockWeight;
	}

	public static List<Integer> getWellDepth(int[][] nextField, int rows, int cols, int[] top) {
		double maxWellDepth = 0;
		List<Integer> wellDepth = new ArrayList<Integer>();
		// near the edge
		if (top[0] < top[1]) {
			wellDepth.add(top[1] - top[0]);
		}
		if (top[cols - 2] > top[cols - 1]) {
			wellDepth.add(top[cols - 2] - top[cols - 1]);
		}
		for (int i = 1; i < cols - 1; i++) {
			if (top[i - 1] > top[i] && top[i] < top[i + 1]) {
				int lowerHeight = top[i - 1];
				if (top[i - 1] < top[i - 2])
					lowerHeight = top[i - 2];
				wellDepth.add(lowerHeight - top[i]);
			}
		}
		return wellDepth;
	}

	public static double getSumOfAllWells(List<Integer> wells) {
		double sum = 0;
		for (int i = 0; i < wells.size(); i++) {
			sum += wells.get(i);
		}
		return sum;
	}
	
	//feature 8
	public static int landingHeight(int[][] nextField, int rows, int cols) {
		int result = 0;
		return result;
	}
	
	//feature 9
	public static int blocks(int[][] nextField, int rows, int cols) {
		int result = 0;
		for (int i=0; i<20; i++) {
			for (int j=0; j<10; j++) {
				if (nextField[i][j] == 0) {
					result++;
				}
			}
		}
		return result;
	}
	
	//feature 10
	public static int getWeightedBlocks(int[][] nextField, int[] top) {
		int weightedBlock = 0;
		for (int i=0; i<10; i++) {
			for (int j=top[i]; j>0; j--) {
				if (nextField[j][i]!=0) {
					weightedBlock += j;
				}
			}
		}
		return weightedBlock;
	}

	//feature 11
	public static int rowTransition(int[][] nextField, int[] top) {
		int result;
		int maxTop = top[0];
		for (int i=0; i<10; i++) {
			if (maxTop < top[i]) {
				maxTop = top[i];
			}
		}
		for (int i=0; i<maxTop; i++) {
			if (nextField[i][0] == 0) {
				result++;
			} 
			if (nextField[i][9]) {
				result++;
			}
			for (int j=1; j<8; j++) {
				if ((nextField[i][j]!=0 && nextField[i][j+1]==0) || (nextField[i][j]==0 && nextField[i][j+1]!=0)) {
					result++;
				}
			}
		}
		return result;
	}

	//feature 12
	public static int colTransition(int[][] nextField, int[] top) {
		int result;
		int maxTop = top[0];
		for (int i=0; i<10; i++) {
			if (maxTop < top[i]) {
				maxTop = top[i];
			}
		}
		for (int j=0; j<10; j++) {
			if (nextField[0][j] == 0) {
				result++;
			} 
			if (nextField[maxTop][j] != 0) {
				result++;
			}
			if (maxTop>1) {
				for (int i=1; i<maxTop-1; i++) {
					if ((nextField[i][j]!=0 && nextField[i+1][j]==0) || (nextField[i][j]==0 && nextField[i+1][j]!=0)) {
						result++;
					}
				}
			}
		}
		return result;
	}
}