#requires install expect module
#sudo apt-get install expect

#show help if required
if [ $1 == "-h" ]
then
  echo "Script for uploading code to SoC computing server & run"
  echo ""
  echo "start.sh <soc_user_id> <password>"
  exit
fi

# parameters
username=$1
password=$2

# list of machines to use
#compg2 is off; compg12 does not exist; access4 requires addtional log in
soc_nodes=( "compg0" "compg1" "compg3" "compg4" "compg5" "compg6" "compg7" "compg8" "compg9" "compg10" "compg11" "compg12" "compg13" "compg14" "compg15" "compg16" "compg17" "compg18" "compg19" "compg20" "compg21" "compg22" "compg23" "compg24" "compg25" "compg26" "compg27" "compg28" "compg29" "compg30" ) # hill-climbing
# soc_nodes=( "compg31" "compg32" "compg33" "compg34" "compg35" "compg36" "compg37" "compg38" "compg39" "compg40" "compg41" "compg42" "compg43" "compg44" "compg45" "compg46" "compg47" "compg48" "compg49" "compg50" "compg51" "compg52" "compg53" "access0" "access1" "access2" "access3" "sma0" "sma1" "sma2" "sma3" "sma4" "sma5" "sma6" "sma7" "sma8" ) #genetic

# clean the folder on computing cluster

# upload the data from the machines
echo "Upload new source"
for machine in "${soc_nodes[@]}"
do
  ./src/upload_to_machine.sh $username $password $machine
done


# ssh to each machine & start running
#dowload the data from the machines
echo "Run code"
for machine in "${soc_nodes[@]}"
do
  ./src/compile_and_run.sh $username $password $machine
done