#requires install expect module
#sudo apt-get install expect

#show help if required
if [ $1 == "-h" ]
then
  echo "Script for downloading data from SoC computing server"
  echo ""
  echo "collect.sh <soc_user_id> <password> <folder_to_download_to>"
  exit
fi

# parameters
username=$1
password=$2
output_folder=$3

list of machines to use
# soc_nodes=( "compg0" "compg01" "compg2" "compg3" "compg4" "compg5" ) # run hill climbing
soc_nodes=( "compg0" "compg1" "compg3" "compg4" "compg5" "compg6" "compg7" "compg8" "compg9" "compg10" "compg11" "compg12" "compg13" "compg14" "compg15" "compg16" "compg17" "compg18" "compg19" "compg20" "compg21" "compg22" "compg23" "compg24" "compg25" "compg26" "compg27" "compg28" "compg29" "compg30" "compg31" "compg32" "compg33" "compg34" "compg35" "compg36" "compg37" "compg38" "compg39" "compg40" "compg41" "compg42" "compg43" "compg44" "compg45" "compg46" "compg47" "compg48" "compg49" "compg50" "compg51" "compg52" "compg53" "access0" "access1" "access2" "access3" "access4" "sma0" "sma1" "sma2" "sma3" "sma4" "sma5" "sma6" "sma7" "sma8" ) # run genetic
# soc_nodes=( "compg0" "compg1" "compg2" "compg3" "compg4" "compg5" "compg6" "compg7" "compg8" "compg9" "compg10" "compg11" "compg13" "compg14" "compg15" "compg16" )


# Clean up, delete all the script's data that we downloaded earlier
echo "Deleting everything in $output_folder"
for machine in "${soc_nodes[@]}"
do
  if [ -d "./$output_folder/$machine" ]
  then
    echo "Deleting data from ./$output_folder/$machine"
    rm -r ./$output_folder/$machine
  else
    echo "Folder not found: ./$output_folder/$machine"
  fi
done

# Dowload the data from the machines
echo "Downloading new data"
for machine in "${soc_nodes[@]}"
do
  ./src/collect_from_machine.sh $username $password $machine $output_folder
done

# Remove all lock files
echo "Removing .lck files"
for machine in "${soc_nodes[@]}"
do
  rm ./$output_folder/$machine/*.lck
done